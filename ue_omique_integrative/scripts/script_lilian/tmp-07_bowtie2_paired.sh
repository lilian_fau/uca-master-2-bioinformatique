#!/bin/bash
#
# bowtie2 version 2.5.2
#
# usage : give as first argument the number of thread to be used

echo "usage : give as first argument the number of thread to be used"
echo
echo -e "\t${0/*\/} nbThreads"
echo

nbThreads=${1:-8}

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/trim_data"
OUTPUT="$WKDIR/mapping_data"

GENOMEDIR="$WKDIR/references/GRCm39_annotation-genome_RefSeq/ncbi_dataset/data/GCF_000001635.27"
INDEXDIR="$GENOMEDIR/bowtie2/GRCm39_genomic_index"
INDEX="$INDEXDIR/GRCm39"

#--------------------------------------------------------------------------------------------------------------------------------

# Make bowtie2 genome indexes

if [ ! -d "$INDEXDIR" ]
then
    echo "bowtie2 genome index not found, making index..."
    mkdir -p "$INDEXDIR"
    genome="$GENOMEDIR/GCF_000001635.27_GRCm39_genomic.fna"
    bowtie2-build --threads 8 "$genome" "$INDEX"
fi


# Set the path for the metadata file in CSV format
metadata=$WKDIR/raw_data/Metadata_PRJNA304160_T0_T18.csv

# Sélection des traitements (saisi par l'utilisateur durant l'exécution du script)
#printf "\nEntrer les conditions utlisées:\n"
#read -p "Condition 1 (0h ou 18h): " CONDITION1
#read -p "Condition 2 (inducible Ikaros ou control): " CONDITION2

# Extract specific fields from metadata based on conditions and filter by "DNase-Hypersensitivity;18h;control$"
#sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${CONDITION1};${CONDITION2}$" | cut -d';' -f1)
sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;0h;inducible Ikaros" | cut -d';' -f1)

# Loop through the list of SRA IDs
for srr in ${sraIds}; do
        echo "_____________ start ${srr} _____________"

        # Measure execution time
        SECONDS=0 
        
        echo "Check for an empty directory..."
        # Check if the output directory exists
        if [ -d "${OUTPUT}/${srr}" ]; then
                # Check if the specific SRA ID directory is not empty
                if [ "$(ls -A ${OUTPUT}/${srr})" ]; then
                        echo "in ${OUTPUT}"
                        echo "${srr}" is not empty and will be cleared
                        #rm -rf "${OUTPUT}/${srr}"
                else
                        echo "in ${OUTPUT}"
                        echo "${srr}" is empty
                fi
        fi

        # Create SRA ID specific output directory
        mkdir -p -m 770 ${OUTPUT}/${srr}

        # Set the output file path
        outfile=${OUTPUT}/${srr}/${srr}_map_trim_paired.bam

        echo "# run mapping of paired reads with bowtie2..."
        # Run bowtie2 to map paired reads and use samtools to convert to BAM format
        bowtie2 --threads ${nbThreads} \
                -x ${INDEX} \
                -1 ${INPUT}/${srr}/${srr}_trimmed_1P.fastq.gz \
                -2 ${INPUT}/${srr}/${srr}_trimmed_2P.fastq.gz |
                samtools view --threads ${nbThreads} \
                -b -o ${outfile}

        echo "files created :"
        # List files in the output directory
        ls ${OUTPUT}/${srr}

        echo -e "_____________ end ${srr} _____________"
        echo nb threads : "${nbThreads}"
        echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
        echo

        # Set permissions for the output BAM file
        chmod 440 ${outfile}

done
