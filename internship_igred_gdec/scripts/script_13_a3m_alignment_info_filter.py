# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 11/04/24

# This code loads a CSV file containing taxonomic data into a pandas DataFrame, filters the rows where the values in
# the "Protein" column match "EFF03", "EFF07", "EFF11", or "EFF18", and where the values in the "Taxon ID" column
# match a specific list of taxonomic identifiers. Then, it writes the filtered data to a new CSV file without quotes
# around the headers of the DataFrame.

# TaxID fungus

# Fusarium culmorum : 5516
# Fusarium poae : 36050
# Fusarium avenaceum : 40199
# Fusarium eqiseti : 61235
# Fusarium subglutinans : 42677
# Fusarium longipes : 694270
# Fusarium tricinctum : 61284
# Fusarium sporotrichoides : 5514
# Fusarium solani : 169388
# Fusarium graminearum : 229533
# Fusarium autroamericanum : 282268
# Venturia inaequalis : 5025
# Pyricularia grisea : 148305
# Claviceps purpurea : 5111
# Neurospora crassa : 5141
# Aspergillus nidulans : 162425
# Ustilago maydis : 5270

# TaxID lignée verte

# Ostreococcus lucimarinus : 242159
# Chlamydomonas reinhardtii : 3055
# Marchantia polymorpha : 1753
# Physcomitrella patens : 3218
# Pinus taeda : 3352
# Picea abies : 3329
# Amborella trichopoda : 13333
# Nymphaea colorata : 210225
# Musa acuminata : 4641
# Ananas comosus : 4615
# Oryza sativa : 4530
# Zea mays : 4577
# Sorghum bicolor : 4558
# Solanum lycopersicum : 4081
# Glycine max : 3847
# Vitis vinifera : 29760
# Prunus persica : 3760
# Theobroma cacao : 3641
# Populus trichocarpa : 3694
# Brassica rapa : 3711
# Arabidopsis lyrata : 59689
# Arabidopsis thaliana : 3702

# PACKAGE-----------------------------------------------------------------------------------------

#python= 3.11.8
#pandas= 2.2.2

import pandas as pd

# CODE--------------------------------------------------------------------------------------------

# Load data from the CSV file into a pandas DataFrame
taxonomy_data = pd.read_csv("/home/lilian/stage/a3m/alignment_results.csv", header=0, sep=",")

# Filter the data for fungi based on specific protein and taxonomic identifiers
filtered_data_fungus = taxonomy_data[taxonomy_data["Protein"].isin(["EFF03", "EFF07", "EFF11", "EFF18"])]
filtered_data_fungus = filtered_data_fungus[filtered_data_fungus["Taxon ID"].isin(["5516", "36050", "40199", "61235", "42677", "694270", "61284", "5514", "169388", "229533", "282268", "5025","148305", "5111", "5141", "162425", "5270"])]

# Extended list (all effectors) & (all Fusarium & Gibberellas)
#filtered_data_fungus = taxonomy_data[taxonomy_data["Protein"].str.contains("EFF")]
#filtered_data_fungus = filtered_data_fungus[filtered_data_fungus["Taxon ID"].isin(["5516", "56646", "36050", "1147111", "48507", "231269", "1208366", "42747", "2364996", "61284", "1053134", "1237068", "2692428", "5507", "5514", "751941", "282569", "48485", "2604345", "56676", "78861", "1328300", "192010", "48495", "195108", "61235", "694270", "78864", "1567544", "47803", "42672", "660122", "179993", "2675880", "1089451", "1028729", "1567541", "229533", "1325734", "1227346", "48490", "131363", "2594811", "2010991", "1325733", "169388", "40199", "5025", "148305", "5111", "5141", "162425", "5270", "5127", "42677", "229533", "42673", "334819", "948311", "5128", "5523"]
)]

# Filter the data for plants based on protein names containing "LOC" and specific taxonomic identifiers
filtered_data_plants = taxonomy_data[taxonomy_data["Protein"].str.contains("LOC")]
filtered_data_plants = filtered_data_plants[filtered_data_plants["Taxon ID"].isin(["242159", "3055", "1753", "3218", "3352", "3329", "13333", "210225", "4641", "4615", "4530", "4577", "4558", "4081", "3847", "29760", "3760", "3641", "3694", "3711", "59689", "3702"])]

# Write the selected data for fungi to a new CSV file without quoting headers
filtered_data_fungus.to_csv("/home/lilian/stage/a3m/filtered_fungus_taxonomy_alignment_results.csv", index=False, quoting=0)
# Write the selected data for plants to a new CSV file without quoting headers
filtered_data_plants.to_csv("/home/lilian/stage/a3m/filtered_plant_taxonomy_alignment_results.csv", index=False, quoting=0)


# Bash verification command (fungus) : grep -E '\b(EFF03|EFF07|EFF11|EFF18)\b.*\b(5516|36050|40199|61235|42677|694270|61284|5514|169388|229533|282268|5025|148305|5111)\b' alignment_results.csv | wc -l
# Bash verification command (plants) : grep -E 'LOC.*\b(242159|3055|1753|3218|3352|3329|13333|210225|4641|4615|4530|4577|4558|4081|3847|29760|3760|3641|3694|3711|59689|3702)\b' alignment_results.csv | wc -l
