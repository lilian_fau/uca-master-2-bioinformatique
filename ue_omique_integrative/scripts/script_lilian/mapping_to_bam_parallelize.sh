#!/bin/bash

#---------- Variables à modifier ----------#

# Déclare un tableau contenant les identifiants SRA
sraIds=("SRR2960371" "SRR2960372" "SRR2960373")

# Définit le répertoire des fichiers de données DNase-seq trimmées
TRIMDIR="/home/RAW_DATA_DNase-seq/lilian_test/3.trimming/"
OUTDIR="/home/RAW_DATA_DNase-seq/lilian_test/5.mapping"

# Définit le répertoire de l'index du génome de référence
INDEXDIR="/home/REFERENCE_GENOME/mm39"
index="${INDEXDIR}/GCF_000001635.27_GRCm39_genomic"

#------------------------------------------#

# Crée le répertoire de sortie s'il n'existe pas, with specific permissions (770)
mkdir -p -m 770 "${OUTDIR}"

# Fonction pour traiter chaque identifiant SRA
process_sra() {
    local srr="$1"
    
    # Utilise un pipeline pour convertir les fichiers SAM en fichiers BAM.gz sans créer de fichiers SAM intermédiaires 
    bowtie2 --threads 4 \
        -x "${index}" \
        -1 "${TRIMDIR}/trimmed_${srr}_1P.fastq.gz" \
        -2 "${TRIMDIR}/trimmed_${srr}_2P.fastq.gz" |
    samtools view -b -o ${OUTDIR}/${srr}_mapped_trim_paired_reads.bam
}

# Parcourt les identifiants SRA et exécute la fonction process_sra en arrière-plan
for srr in "${sraIds[@]}"; do
    process_sra "$srr" &
done

# Attend que tous les processus en arrière-plan se terminent
wait
