#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 1 --snakefile 12_count_matrix_data.smk
# This twelfth script generates the count matrix required for R analyses using the Bedtools multicov tool.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        "../results/rawcounts_DNaseSeq_SD.tsv"

rule multicoverage:
    input:
        bed="../data/6.r_analysis/all_merged.bed",
        bam_file=expand("../data/4.mapping/sorted_bam/sorted_{sample}.bam", sample=config["samples_compiled"]),
    output:
        counts="../results/rawcounts_DNaseSeq_SD.tsv"
    resources:
        mem_mb=15,  
        time="00:10:00"
    #log: "../log/12_count_matrix_data/matrix_count.log"    
    shell:
        """
        ml gcc/4.8.4 bedtools/2.27.1
        bedtools multicov -bams {input.bam_file} -bed {input.bed} > {output.counts}
        cp ../results/rawcounts_DNaseSeq_SD.tsv ../data/6.r_analysis/
        """


