# Lecture du contenu du Fichier 1
with open('../datas/monomer_5367c/input_fasta/summary_sequences.fasta', 'r') as file1:
    lines_file1 = file1.readlines()

# Création d'un dictionnaire pour stocker la correspondance entre les identifiants et les gènes du Fichier 1
id_gene_mapping = {}
for line in lines_file1:
    if line.startswith('>'):
        parts = line.split('[gene=')
        LOC_ID_file_1 = parts[1].split(']')[0]
        UPI_ID_file_1 = parts[0][1:-1]
        id_gene_mapping[UPI_ID_file_1] = LOC_ID_file_1

# Lecture du contenu du Fichier 2
with open('../blast/results/LOC_orthologous_list.txt', 'r') as file2:
    lines_file2 = file2.readlines()

# Création de la sortie souhaitée en utilisant le dictionnaire de correspondance
output_lines = []
for line in lines_file2:
    UPI_ID_file_2 = line.split('\t')
    gene_id1 = id_gene_mapping.get(UPI_ID_file_2[0])   
    gene_id2 = id_gene_mapping.get(UPI_ID_file_2[1])
    output_line = f"{gene_id1}\t{gene_id2}\t{ids[2]}\t{ids[3]}\t{ids[4]}\t{ids[5]}\t{ids[6]}\t{ids[7]}\t{ids[8]}\t{ids[9]}\t{ids[10]}\t{ids[11]}"
    output_lines.append(output_line)

# Écriture de la sortie dans un nouveau fichier
with open('convert.fasta', 'w') as output_file:
    for line in output_lines:
        output_file.write(line)
