![UCA logo](https://www.uca.fr/medias/photo/logo-uca-long-300dpi_1493730258077-png)

# UE - Analyse de données médicales et Deep Learning

#### Author: Lilian Faurie - Master 2 Bioinformatique
#### Date: 04/01/2024

Segmentation par Deep Learning CNN de l'endocarde ou de l’épicarde sur des coupes de ciné IRM 2D petit axe.

## Description de la base de données
Les images 3D+t de la base de données de 30 patients ont été converties en coupes .png avec comme nom de fichier : nn_zz_tt.png où nn est le numéro du patient demandé à l'utilisateur, zz le niveau de coupe normalisé entre 0 et 1 et tt le temps dans le cycle cardiaque lui aussi normalisé entre 0 et 1. Ces images et les annotations des années précédentes sont disponibles au téléchargement sous la forme de trois archives : frames_all.zip pour les images, masks_endo.zip pour les contours endocardiques et masks_epi.zip pour les contours épicardiques.

## Implémentation de la méthode d’apprentissage
Pour implémenter le CNN, on utilisera la bibliothèque Python Keras qui est une surcouche de TensorFlow. Vous devez avoir Python installé sur votre ordinateur (version 3.9.2) avec les packages Tensorflow, numpy, scikit-image et Matplotlib. Dans la salle informatique 643, toutes ces bibliothèques font partie d’un environnement de développement Anaconda. Nous utiliserons Visual Studio Code (ou Codium) comme IDE. Pour qu’il soit compatible avec Anaconda, il faut le lancer depuis une fenêtre d’invite Anaconda (lien Anaconda prompt) avec l’instruction « code . » ou « codium . ». Les images natives et les masques correspondants sont supposés stockés dans deux sous-répertoires « frames » et « masks ».

Trois étapes doivent être implémentées successivement : 
> la création et la structuration des données (make_data.py) : construction de trois ensembles de données (apprentissage, validation, test) en sélectionnant un intervalle de niveaux de coupe et un intervalle de temps, puis mise en forme des images (resize 128x128 en utilisant skimage)

> l’apprentissage avec génération des données par augmentation (train.py) : on utilisera ImageDataGenerator de Keras pour augmenter les ensembles d'apprentissage et de validation (attention les images générées seront 2 fois plus grandes que les images initiales du fait des transformations géométriques appliquées). On utilisera dans un premier temps le modèle de CNN le plus connu à savoir UNet (décrit dans le fichier model.py) et on réalisera l’apprentissage du modèle, avec sauvegarde des poids correspondants (weights.h5). Cet article présente différentes fonctions de perte utilisées en segmentation.

> inférence à partir du modèle estimé (predict.py) : prédiction sur les trois ensembles et sauvegardes des images segmentées de l'ensemble de test 

## Analyse des résultats de prédiction
On pourra par exemple étudier l'influence du nombre d'images de la base d'apprentissage et du choix du niveau de coupe (local ou global) et du temps dans le cycle (local ou global). Pour objectiver le résultat de la segmentation, on pourra utiliser la valeur de la fonction de perte (coefficient de Dice par exemple) ou bien définir de nouvelles métriques permettant une comparaison des contours obtenus (https://loli.github.io/medpy/metric.html).

Les modèles pré-entraînés sont stockés sous le format Weights_floss_zmin_zmax_nepoch_batchsize_valloss.h5 avec floss la fonction de perte utilisée, zmin niveau de coupe minimum, zmax niveau de coupe maximum, nepoch nombre d'epochs, batchsize taille des batchs, valloss valeur de la fonction de perte finale sur l'ensemble de validation sur le Drive UCA : download. Les fichiers .csv de log, ainsi que les archives des répertoires utiles à Tensorboard porteront les mêmes noms. Pour utiliser Tensorboard, l’installer avec pip, puis le lancer avec la ligne de commande suivante : python -m tensorboard.main –logdir ‘chemin absolu vers le répertoire’
