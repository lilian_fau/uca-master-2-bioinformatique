#!/bin/bash
#
# bedtools version v2.31.0 
#
# perform intersection of peakcalling file and ikaros TFBS regions


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/peak_data"
OUTPUT="$WKDIR/intersect_data"

peaks_data=($(find $INPUT -type f -name "*.narrowPeak"))
ikaros="$WKDIR/references/lymphoB_mm_IKZF1_peaks.bed"

#--------------------------------------------------------------------------------------------------------------------------------

# Iterate over each narrowPeak file
for peak_file in "${peaks_data[@]}"; do
    # Get the filename without the path and extension
    filename=$(basename -- "$peak_file")
    filename_no_ext="${filename%%_*}"

    # Set the output file path for the intersection
    outfile1="$OUTPUT/${filename_no_ext}_intersect_IKZF1.bed"
    outfile2="$OUTPUT/${filename_no_ext}_IKZF1_tfbs_open.bed"

    echo "Making intersection for: $filename"

    # Use bedtools intersect to find the common regions between the current narrowPeak file and ikaros file
    bedtools intersect -a "$peak_file" -b "$ikaros" > "$outfile1"
    bedtools intersect -a "$ikaros" -b "$peak_file" > "$outfile2"

    echo file created :
    # List the created output files
    ls "${outfile1}"
    ls "${outfile2}"
done

echo "Analysis complete. Time elapsed: $SECONDS seconds."


