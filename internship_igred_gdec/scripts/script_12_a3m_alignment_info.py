# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 10/04/24
# This code is a Python script designed to process protein sequence alignment files in the .a3m format. Here's a summary of its main features:

# 1. Alignment data extraction:
#    The script traverses each .a3m file in a specified folder. It extracts the protein name from the file name and retrieves the alignment sequences from this file.

# 2. Sequence cleaning:
#    The extracted sequences are cleaned to remove unwanted words, such as contig identifiers or special characters.

# 3. Parallel sequence processing:
#    The cleaned sequences are processed in parallel to speed up the process, thanks to the multiprocessing library.

# 4. Retrieval of taxonomic information:
#    For each sequence, the script retrieves taxonomic information from a database (UniProt or UniRef) using the associated accession identifiers.

# 5. Writing results to a CSV file:
#    The results, including the protein name, accession identifier, identity, e-value, UniParc identifier, scientific name, and taxon ID, are written to a CSV file. The aligned sequences are also included.

# 6. Concurrency management:
#    The script utilizes concurrency management to optimize the processing of files in parallel, making the most of available resources.

# 7. Cache management:
#    To avoid redundant requests, the script uses a cache to store data already retrieved from databases.

# 8. Execution time:
#    The script measures the overall execution time and displays it at the end of execution.

# PACKAGES----------------------------------------------------------------------------------------

#python= 3.11.8
#requests= 2.31.0
#multiprocessing= 2.6.2.1

import os
import csv
import requests
from multiprocessing import Pool, cpu_count
import time

# FUNCTIONS---------------------------------------------------------------------------------------

# Function to process each file
def process_file(file_path):
    """
    Process each file in the given path.

    Args:
        file_path (str): Path to the file to be processed.

    Returns:
        tuple: A tuple containing the protein name and a dictionary of processed sequences.
    """
    # Extract protein name and sequences from file
    protein_name_target, sequences_target = alignment_sequences(file_path)
    # Clean sequences
    clean_sequences_target = clean_sequences(sequences_target)
    # Process sequences in parallel
    with Pool(processes=cpu_count()) as pool:
        processed_sequences = pool.map(process_sequence, clean_sequences_target.items())
    processed_sequences_dict = dict(processed_sequences)
    return protein_name_target, processed_sequences_dict


# Function to extract protein name and sequences from alignment file
def alignment_sequences(file_path, flag_target='>101'):
    """
    Extract protein name and sequences from the alignment file.

    Args:
        file_path (str): Path to the alignment file.
        flag_target (str): Flag to identify target sequences.

    Returns:
        tuple: A tuple containing the protein name and a dictionary of sequences.
    """
    file_name = os.path.basename(file_path)
    protein_name_target = file_name.split(".")[0]

    sequences_target = {}
    current_key = None  # Initialize current_key to None

    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()

            if line.startswith('>'):
                # Extract identity and e-value
                parts = line.split("\t")
                if len(parts) >= 4:
                    identity_evalue = ', '.join(parts[2:4])
                else:
                    identity_evalue = 'NA, NA'  # Replace with "NA, NA" if information is not available

                line = parts[0]
                current_key = line.replace(flag_target, protein_name_target).lstrip(">") + ", " + identity_evalue
            elif current_key is not None:
                sequences_target[current_key] = line
    return protein_name_target, sequences_target


# Function to clean sequences based on specified words to remove
def clean_sequences(sequences_target, words_to_remove=(
'MAG', 'SRR', 'ERR', 'MGYP', 'scaffold', 'contig', '|', '.a:Ga', 'PeaSoi', 'OM-RGC', 'Deeta', 'uvig_')):
    """
    Clean sequences by removing unwanted words.

    Args:
        sequences_target (dict): Dictionary of sequences to be cleaned.
        words_to_remove (tuple): Words to be removed from sequences.

    Returns:
        dict: Dictionary of cleaned sequences.
    """
    clean_sequences_target = {k: v for k, v in sequences_target.items() if
                              not any(word in k for word in words_to_remove)}
    return clean_sequences_target


# Function to get taxonomy information based on accession
def get_taxonomy(accession):
    """
    Get taxonomy information based on accession.

    Args:
        accession (str): Accession ID.

    Returns:
        tuple: A tuple containing taxonomy information.
    """
    if "UniRef100_" in accession:
        accession = accession.split(",")[0]
        return get_uniref_taxonomy(accession)
    else:
        accession = accession.split(",")[0]
        return get_uniprot_taxonomy(accession)


# Function to get taxonomy information from UniRef database
def get_uniref_taxonomy(accession):
    """
    Get taxonomy information from UniRef database.

    Args:
        accession (str): Accession ID.

    Returns:
        tuple: A tuple containing taxonomy information.
    """
    # Check cache for previously fetched data
    if accession in cache:
        return cache[accession]

    url = f"https://rest.uniprot.org/uniref/{accession}"
    response = session.get(url)

    try:
        response.raise_for_status()
        data = response.json()

        extra_attributes = data.get('representativeMember', {})
        scientific_name = extra_attributes.get('organismName', 'NA')
        # Select only what's before the second space
        scientific_name = ' '.join(scientific_name.split(' ')[:2])
        taxon_id = extra_attributes.get('organismTaxId', 'NA')

        if "UniRef100_UPI" in accession:
            uniParcId = extra_attributes.get('memberId', 'NA')
        else:
            uniParcId = extra_attributes.get('uniparcId', 'NA')

        result = (uniParcId, scientific_name, taxon_id)
        cache[accession] = result
        return result

    except Exception as e:
        return f"Error retrieving data for {accession}: {e}"


# Function to get taxonomy information from UniProt database
def get_uniprot_taxonomy(accession):
    """
    Get taxonomy information from UniProt database.

    Args:
        accession (str): Accession ID.

    Returns:
        tuple: A tuple containing taxonomy information.
    """
    # Check cache for previously fetched data
    if accession in cache:
        return cache[accession]

    url = f"https://rest.uniprot.org/uniprotkb/{accession}"
    response = session.get(url)

    try:
        response.raise_for_status()
        data = response.json()

        common_taxon = data.get('organism', {})
        scientific_name = common_taxon.get('scientificName', 'NA')
        # Select only what's before the second space
        scientific_name = ' '.join(scientific_name.split(' ')[:2])
        taxon_id = common_taxon.get('taxonId', 'NA')

        extra_attributes = data.get('extraAttributes', {})
        uniParcId = extra_attributes.get('uniParcId', 'NA')

        result = (uniParcId, scientific_name, taxon_id)
        cache[accession] = result
        return result

    except Exception as e:
        return f"Error retrieving data for {accession}: {e}"


# Function to process each sequence, including fetching taxonomy information
def process_sequence(accession_sequence):
    """
    Process each sequence, including fetching taxonomy information.

    Args:
        accession_sequence (tuple): A tuple containing accession ID and sequence.

    Returns:
        tuple: A tuple containing accession ID and dictionary of processed sequence and taxonomy information.
    """
    accession, sequence = accession_sequence
    taxonomy = get_taxonomy(accession)
    return accession, {'sequence': sequence, 'taxonomy': taxonomy}


# Function to write results to a CSV file
def write_to_csv(results, csv_file, protein_name_target):
    """
    Write results to a CSV file.

    Args:
        results (dict): Dictionary containing processed results.
        csv_file (str): Path to the CSV file.
        protein_name_target (str): Protein name.

    Returns:
        None
    """
    # Check if CSV file exists
    file_exists = os.path.isfile(csv_file)

    with open(csv_file, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')  # Set delimiter as ","

        # Write header only if file is empty
        if not file_exists:
            writer.writerow(['Protein', 'Accession', 'Identity', 'e-value', 'UniParcID', 'Scientific Name', 'Taxon ID',
                             'Alignment'])

        # Write data to CSV
        for protein, sequences in results.items():
            for accession, data in sequences.items():
                id = accession.split(",")[0]
                identity = accession.split(",")[1]
                evalue = accession.split(",")[2]

                # Check if any part of the taxonomy is 'NA'
                if 'NA' in data['taxonomy']:
                    continue  # Skip writing this line if any part of the taxonomy is 'NA'

                # Check conditions for writing the line
                is_sequence_101 = accession.startswith(protein_name_target)
                if not (is_sequence_101) and any(
                        part == 'E' or part == 'r' or part == 'NA' for part in data['taxonomy']):
                    continue  # Skip writing this line if it's not for sequence 101 or 102 and contains 'E' or 'r'

                # Write the row to the CSV file
                writer.writerow([protein_name_target, id, identity, evalue, data['taxonomy'][0], data['taxonomy'][1],
                                 data['taxonomy'][2], "'" + data['sequence'] + "'"])


# Initialize requests session and cache
session = requests.Session()
cache = {}

# Main script execution
if __name__ == '__main__':
    start_time = time.time()

# CONFIGURATION-----------------------------------------------------------------------------------

    alignment_folder = "/home/lilian/stage/a3m/a3m_datas"

# ------------------------------------------------------------------------------------------------

    file_names = [f for f in os.listdir(alignment_folder) if f.endswith('.a3m')]
    file_names.sort()
    total_files = len(file_names)

    print(
        f"\n-----------------------------------------\nNumber of file(s) (.a3m): {len(file_names)}\n-----------------------------------------\n")

    results = {}
    pool = Pool(processes=cpu_count())

    # Initialize processed files counter
    processed_files_count = 0

    for file_name in file_names:
        processed_files_count += 1
        file_path = os.path.join(alignment_folder, file_name)
        print(f"Processing file {processed_files_count}/{total_files}:", file_name)

        # Process each file
        protein_name_target, processed_sequences = process_file(file_path)
        results[protein_name_target] = processed_sequences

        # Write results to CSV file
        csv_file = "alignment_results.csv"
        write_to_csv(results, csv_file, protein_name_target)

        # Clear results dictionary for next iteration
        results = {}
        print(f" | {file_name} results have been written to:", csv_file, "\n")

    # Close the pool of processes
    pool.close()
    pool.join()

    end_time = time.time()
    print("Script execution time: ", end_time - start_time, " seconds.")

# ------------------------------------------------------------------------------------------------