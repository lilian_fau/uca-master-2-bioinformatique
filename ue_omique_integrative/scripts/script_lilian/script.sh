#!/bin/bash


# FAURIE Lilian M2BI
# Excution du script avec la commande: bash -i nom_du_script.sh




# Création de l'arborescence 
mkdir -p {1.raw,2.multiqc_raw,3.trimming,4.multiqc_trimming,5.mapping,6.peak_calling}


# Variables des dossiers
dir_1_raw=$"1.raw"
dir_2_multiqc_raw=$"2.multiqc_raw"
dir_3_trim=$"3.trimming"
dir_4_multiqc_trim=$"4.multiqc_trimming"
dir_5_mapping=$"5.mapping"
dir_6_peak=$"6.peak_calling"

# Variables des environnements
conda_env_1=$"omiqueTools"
conda_env_2=$"macs3-bowtie2"

# Variables des packages des environnements
conda_env1_packages=("sra-tools" "fastqc" "multiqc" "trimmomatic" "samtools" "bedtools")
conda_env2_packages=("bowtie2" "samtools" "macs3")




echo -e "\n\n------Installation de l'environnement-------\n\n"

eval "$(conda shell.bash hook)"

# Vérifier si l'environnement 'omiqueTools' en python 3.12 existe ou le créer
echo -e "\nCréation de l'environnement en cours ($conda_env_1)"

if ! conda env list | grep -q $conda_env_1; then
    conda create -n $conda_env_1 python=3.12.0 -y
fi
echo -e "Environnement $conda_env_1 : OK\n"

# Vérifier si l'environnement 'macs3-bowtie2' en python 3.8.18 existe ou le créer
echo -e "Création de l'environnement en cours ($conda_env_2)"
if ! conda env list | grep -q $conda_env_2; then
    conda create -n $conda_env_2 python=3.8.18 -y
fi
echo -e "Environnement $conda_env_2 : OK\n"

conda init bash > /dev/null

# Installation des packages pour l'environnement 'omiqueTools' 
echo "Packages requis ($conda_env_1)"
conda activate $conda_env_1

# Liste des paquets requis
paquets_requis=("${conda_env1_packages[@]}")

# Installer les paquets manquants
paquets_manquants=()
for paquet in "${paquets_requis[@]}"; do
    conda list -n "$conda_env_1" | grep -q "^$paquet " || paquets_manquants+=("$paquet")
done

# Installer les paquets manquants
if [ ${#paquets_manquants[@]} -gt 0 ]; then
    echo -e "Installation des paquets manquants : ${paquets_manquants[@]}"
    conda install "${paquets_manquants[@]}" -y
else
    echo -e "Tous les packages requis pour $conda_env_1 sont déjà installés : OK\n"
fi
conda deactivate




# Installation des packages pour l'environnement 'macs3-bowtie2' 
echo -e "Packages requis ($conda_env_2)"
conda activate $conda_env_2

# Liste des paquets requis
paquets_requis=("${conda_env2_packages[@]}")

# Installer les paquets manquants
paquets_manquants=()
for paquet in "${paquets_requis[@]}"; do
    conda list -n "$conda_env_2" | grep -q "^$paquet " || paquets_manquants+=("$paquet")
done

# Installer les paquets manquants
if [ ${#paquets_manquants[@]} -gt 0 ]; then
    echo -e "Installation des paquets manquants : ${paquets_manquants[@]}"
    conda config --add channels maximinio
    conda install "${paquets_manquants[@]}" -y
else
    echo -e "Tous les packages requis pour $conda_env_2 sont déjà installés : OK\n"
fi

conda deactivate




echo -e "\n\n------------Obtention des SRR--------------\n\n"

while true; do
    # Traitement du fichier Metadata_PRJNA304160_T0_T18.txt afin de le formater en fichier tabulé
    sed 's/,/;/g' Metadata_PRJNA304160_T0_T18.txt > Metadata_PRJNA304160_T0_T18.csv

    # Sélection des traitements (saisi par l'utilisateur durant l'exécution du script)
    printf "Entrer les traitements pour déterminer les SRR à télécharger:\n"
    read -p "Traitement 1 (0h, 18h, inducible Ikaros ou control): " treatment1
    read -p "Traitement 2 (0h, 18h, inducible Ikaros ou control): " treatment2

    # Stockage des identifiants SRA correspondant aux traitements
    idSRA=$(cat Metadata_PRJNA304160_T0_T18.csv | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${treatment1};${treatment2}$" | cut -d';' -f1)

    if [ -n "$idSRA" ]; then
        printf "\nIdentifiants SRA correspondants:\n"
        echo -e "$idSRA"
        break  # Sort de la boucle si des identifiants SRA sont trouvés
    else
        echo -e "Aucune correspondance trouvée. Veuillez saisir des traitements valides."
    fi
done




# Traitement n°1
# Téléchargement des fichiers SRR correspondant aux identifiants SRA

echo -e "\n\n---------Téléchargement des SRR------------\n\n"

conda activate $conda_env_1

total=$(echo $idSRA | wc -w)
counter=0

for SRA in $idSRA; do
    ((counter++))
    
    # Vérifier si les deux fichiers existent déjà dans le dossier
    if [ -e "$dir_1_raw/${SRA}_1.fastq.gz" ] && [ -e "$dir_1_raw/${SRA}_2.fastq.gz" ]; then
        echo -e "${SRA}.fastq.gz existe déjà dans le dossier. Pas de téléchargement nécessaire."
        continue  # Passer à l'itération suivante de la boucle
    fi
    
    # Calculer le pourcentage de progression
    progress=$((counter * 100 / total))
    
    # Afficher la barre de progression
    echo -e "Progression du téléchargement ($counter/$total fichiers): $progress%\n"
    
    # Afficher le message de début du téléchargement
    echo -e "\rTéléchargement de $SRA en cours..."
    
    # Téléchergement en mode split et stockage dans le dossier "1.raw"
    fastq-dump --gzip --split-3 --accession $SRA --outdir $dir_1_raw
    
    # Afficher le message de fin du téléchargement
    echo -e "\nTéléchargement de $SRA terminé.\n"
    
done

echo -e "Tous les téléchargements sont terminés ! : OK"




# Traitement n°2
# Multiqc des fichiers 

echo -e "\n\n-------------Fastqc & Multiqc--------------\n\n"

# Vérifier si des fichiers HTML existent dans le répertoire
html_files=($dir_2_multiqc_raw/*.html)
if [ ${#html_files[@]} -gt 0 ]; then
    echo "Les fichiers HTML existent déjà. FastQC sera ignoré."
else
    # Exécuter FastQC sur les fichiers FASTQ en entrée
    fastqc -t $threads -o $dir_2_multiqc_raw $dir_1_raw/*.fastq.gz
    echo -e "FastQC : OK\n"
fi

multiqc_files=($dir_2_multiqc_raw/multiqc_report*.html)
if [ ${#multiqc_files[@]} -gt 0 ]; then
    echo "Le multiqc existent déjà. Multiqc sera ignoré"
else
    multiqc $dir_2_multiqc_raw/*_fastqc.zip -o $dir_2_multiqc_raw
    echo -e "MultiQC : OK"
fi

echo -e "Analyses qualité : OK"




# Traitement n°3
# Nettoyage des reads

echo -e "\n\n-----------Nettoyage des données-----------\n\n"

# Répertoire où se trouve le fichier d'adaptateur TruSeq.
adapter_dir="/home/SHARED_FILES/adapters_TruSeq3-PE.fa"


for SRA in $idSRA; do

 output_1P="$dir_3_trim/trimmed_"$SRA"_1P.fastq.gz"
 output_1U="$dir_3_trim/trimmed_"$SRA"_1U.fastq.gz"
 output_2P="$dir_3_trim/trimmed_"$SRA"_2P.fastq.gz"
 output_2U="$dir_3_trim/trimmed_"$SRA"_2U.fastq.gz"

 if [ ! -f "$output_1P" ] || [ ! -f "$output_1U" ] || [ ! -f "$output_2P" ] || [ ! -f "$output_2U" ]; then

    # Commande trimmomatic (4 thread CPU)
    trimmomatic PE \
      -threads 4 \
      $dir_1_raw/"$SRA"_1.fastq.gz \
      $dir_1_raw/"$SRA"_2.fastq.gz \
      $dir_3_trim/trimmed_"$SRA"_1P.fastq.gz \
      $dir_3_trim/trimmed_"$SRA"_1U.fastq.gz \
      $dir_3_trim/trimmed_"$SRA"_2P.fastq.gz \
      $dir_3_trim/trimmed_"$SRA"_2U.fastq.gz \
      ILLUMINACLIP:"$adapter_dir":2:30:10 \
      LEADING:5 TRAILING:5 SLIDINGWINDOW:4:5 MINLEN:25
      
    echo -e "$SRA nettoyé\n"
 else
    echo -e "$SRA déjà nettoyé, pas de réexécution"
 fi
done

echo -e "Nettoyage des données terminées : OK"

echo -e "\n\n-------------------------------------------\n\n"

conda deactivate
