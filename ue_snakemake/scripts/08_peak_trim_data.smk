#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 14 --snakefile 08_peak_trim_data.smk
# This eighth script performs peaks calling with Homer's findPeaks tool to identify areas of the genome that have been enriched. The sam files generated previously are used in this step. 
# We could have used compressed bam files directly, but we would have needed to install samtools in order for Homer to work with this format. We later compress these files to save space.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/5.peak_calling/{sample}", sample=config["samples_compiled"]),

rule create_conda_env:
    conda:
        "envs/08_peak_trim_data.yaml"
    shell:
        """
        if ! conda env list | grep -q peak_calling; then
             conda env create -f envs/08_peak_trim_data.yaml
        fi
        """

rule homer_narrow_500_peaks:
    input:
        sam="../data/4.mapping/trimmed_{sample}_mapping.sam"
    output:
        directory("../data/5.peak_calling/{sample}")
    conda:
        "envs/08_peak_trim_data.yaml"
    resources:
        mem_mb=500,  
        time="00:10:00"
    #log: "../log/08_peak_trim_data/peak_{sample}.log"
    shell:
        """
        eval "$(conda shell.bash hook)"
        conda activate peak_calling
        makeTagDirectory {output} {input.sam} -format sam
        findPeaks {output} -size 150 -minDist 50 fdr 0.01 -style factor -o {output}/{wildcards.sample}.narrow.peaks
        awk -v OFS='\t' '{{print $2,$3,$4}}' {output}/{wildcards.sample}.narrow.peaks | tail -n+36 > {output}/{wildcards.sample}.narrow.peaks.bed
        findPeaks {output} -size 500 -minDist 50 -fdr 0.01 -style factor -o {output}/{wildcards.sample}.500.peaks
        awk -v OFS='\t' '{{print $2,$3,$4}}' {output}/{wildcards.sample}.500.peaks | tail -n+36 > {output}/{wildcards.sample}.500.peaks.bed
        bedops -m {output}/{wildcards.sample}.narrow.peaks.bed {output}/{wildcards.sample}.500.peaks.bed > {output}/{wildcards.sample}.merge.peaks
        """


