#!/bin/bash
#
# bedtools version v2.31.0 
#
# perform intersection of peakcalling file and ikaros TFBS regions


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/peak_data"

#--------------------------------------------------------------------------------------------------------------------------------

tab=($(ls -d "$INPUT"/*)) # exp. dir. paths

# test on control0, intersect in 2 rep...
echo "test on control0, intersect in 2 rep..."
for EXP in ${tab[@]}
do
		# peaks files array
		tab=($(find "$EXP" -type f -name "*_peaks.narrowPeak" |sort))
		
		# peaks files array sorted by reverse # peaks
		peaks=($(wc -l ${tab[@]} |
			awk -vOFS="\t" '{print $1, $2}' |
			sort -k1nr|
			cut -f2 |
			grep -v "total"))

		name=${EXP/*\/}
		outfile="${INPUT}/${name}_dhs.bed"

		echo "peak intersect with min 1bp overlap in ${name}..."

		# get peaks in in file with max nb peaks that overlap at least 1 peack in
		# an other replicate
		# (remove peaks in chrUn..., chrM and chr...random)

		bedtools intersect -a <(cat "${peaks[0]}" | grep -v -P "(chrUn)|(.*random)|(chrM)") \
				-u \
				-b <(cat "${peaks[1]}" | grep -v -P "(chrUn)|(.*random)|(chrM)") \
				-b <(cat "${peaks[2]}" | grep -v -P "(chrUn)|(.*random)|(chrM)") > "$outfile"

		echo "file created :"
		ls "${outfile}"
		echo
done
