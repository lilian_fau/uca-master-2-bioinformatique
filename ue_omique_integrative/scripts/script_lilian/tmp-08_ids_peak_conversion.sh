# file with refseq (NC...) and ucsc (chr...) chromosome names symmetry
# downloaded here
# https://hgdownload.soe.ucsc.edu/goldenPath/mm39/bigZips/mm39.chromAlias.txt

# WORKING DIRECTORY ---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/peak_data"
OUTPUT="$WKDIR/peak_data"

bed_data=($(find "$INPUT" -type f -name "*.narrowPeak"))
chrnames="/home/data_local/references/GRCm39_annotation-genome_RefSeq/mm39.chromAlias.txt"

# --------------------------------------------------------------------------------------------------------------------------------

# Iterate over each narrowPeak file
for bed in "${bed_data[@]}"; do
    filename=$(basename -- "$bed")
    filename_no_ext="${filename%.*}"

    # Join and format data
    join -1 2 <(tail -n +2 "$chrnames" | cut -f1,5 | sort -k2,2) -2 1 <(sort -k1 "$bed") |
        cut -d" " -f2- | tr " " "\t" > "$OUTPUT/$filename_no_ext".narrowPeak  # Adjust the output path as needed

    # Check the first few lines of the joined data
    join -1 2 <(tail -n +2 "$chrnames" | cut -f1,5 | sort -k2,2) -2 1 <(sort -k1 "$bed") | head

    # Check the first few lines of the formatted data
    join -1 2 <(tail -n +2 "$chrnames" | cut -f1,5 | sort -k2,2) -2 1 <(sort -k1 "$bed") |
        cut -d" " -f2- | tr " " "\t" | head

done
