![UCA logo](https://www.uca.fr/medias/photo/logo-uca-long-300dpi_1493730258077-png)

# Projet d'Omique intégrative 2023

## Date: Décembre 2023

**Auteurs - Groupe 2** : Mathieu CAMMAS, Seydina DIOUF, Lilian FAURIE, Lucile MARTIN 

### Objectif
Ce projet a pour objectif de faire de l'intégration de données RNA-seq, DNAse-seq et RRBS-seq, dans le but d'étudier l'influence du gène Ikaros dans la différenciation cellulaire des lymphocytes B. 

Question scientifique : **Recherche de gènes cibles, dans les régions accessibles et déméthylées de l'ADN, du facteur de transcription _Ikaros_ lors de la différenciation des lymphocytes B**. 

## Déroulement du projet

Lors de la réalisation de ce projet, les différents jeux de données : T0_control, T0_ikaros, T18_control et T18_ikaros ont été répartis entre les différentes personnes du groupe et traitées indépendamment, avec des commandes similaires.

Pour respecter un pipeline FAIR, des scripts automatisés **/scripts/workflow_dnase-seq_scripts/** ont été mis en place pour traiter n'importe quel jeu de données et vérifier l'interopérabilité des scripts réalisés par chacun. 

#### Répartition des tâches

L'optimisation du pré-traitement des données à été le premier point crucial pour organiser au mieux la gestion du projet :
- 4 conditions expérimentales : control_T0, control_T18, ikaros_T0, ikaros_T18
- 3 réplicas biologiques par condition

Les 12 fichiers fastq.gz ont donc été répartis à chaque membre du groupe qui les a traités indépendamment, en utilisant les mêmes commandes d'execution : 
- Mathieu : control_T0
- Lilian : control_T18
- Seydina : ikaros_T0
- Lucile : ikaros_T18

A la fin du pré-traitement, les tâches ont été réparties différement :
- Analyses et traitement des données DNAse-seq : Mathieu (+), Lilian et Seydina.
- Intégration des données RNA-seq : Lucile (+)
- Intégration des données RRBS-seq : Seydina (+)
- Environnement GitLab, automatisation des scripts : Lilian (+), Lucile, Seydina et Mathieu

# Partie Programmation
##### Connexion SSH au serveur

Pour se connecter au serveur depuis un nouvel environnement de travail, il faut :

- Génerer une clé SSH public si on n'en n'a pas : 
```
$ ssh-keygen
$ cat .ssh/id_rsa.public # copier la cle
$ cp -r .ssh/ /home/
```
- Mettre la clé sur le serveur ubuntu : 
```
$ cd /home/ubuntu
$ sudo nano .ssh/authorized_keys # coller la cle
```
- Se connecter au serveur : 
```
$ ssh -A ubuntu@193.49.167.17
```

## Informations sur le stockage et les données

Accession à l'espace de stockage des données : 

- Se connecter sur la machine virtuelle de la FAC, les données sont stockées dans : 

```
/Public/Bronner/scratch/
```

```
[/Public/Bronner/scratch/] $ tree
.
├── DNase-seq
│   ├── consensus_dhs
│   ├── intersect_data
│   ├── mapping_data
│   ├── open_region_ikzf
│   ├── Peak-Calling
│   │   ├── control0
│   │   ├── control18
│   │   ├── ikaros0
│   │   └── ikaros18
│   ├── Quality-Control
│   │   ├── control0
│   │   │   ├── raw
│   │   │   └── trimmed
|   |      ...
│   ├── references
│   │   └── Ikaros_binding_sites
│   └── Trimming
│   	├── control0
│   	│   ├── SRR2960359
│   	│   ├── SRR2960360
│   	│   └── SRR2960361
│   	├── control18
│   	├── ikaros0
│   	└── ikaros18
├── RNA-seq
│   ├── DESeq2
│   ├── htseq
│   └── Mapping
│   	├── control0
│   	├── control18
│   	├── ikaros0
│   	├── ikaros18
│   	└── log
└── RRBS
	├── Alignement_Bismark
	├── Methylkit
	│   ├── merged
	│   │   ├── hyper
	│   │   └── hypo
	│   ├── methylKit_0h_100
	│   ├── methylKit_0h_1000
	│   ├── methylKit_0h_500
	│   ├── methylKit_18h_100
	│   ├── methylKit_18h_1000
	│   ├── methylKit_18h_500
	│   ├── methylKit_control_100
	│   ├── methylKit_control_1000
	│   ├── methylKit_control_500
	│   ├── methylKit_ikaros_100
	│   ├── methylKit_ikaros_1000
	│   └── methylKit_ikaros_500
    └── README.txt
```
## Pipeline d'analyse

Les commandes sont accessibles dans le dossier workflow_dnase-seq_scripts/

L'execution des scripts nécessite la mise en place d'environnements conda contenant : 
```
$ conda create -n omiqueTools
```
- Packages dans omiqueTools : 
bedtools v2.31.1, fastqc v0.12.1, multiqc v1.14, samtools v1.18, sra-tools v3.0.9, trimmomatic v0.39
```
$ conda create -n macs3-bowtie2
```
- Packages dans macs3-bowtie2 : 
bowtie2 v2.5.2, macs3 v3.0.0a6 (via maximinio), samtools v1.18

### Ressources 

Voici les différentes ressources utilisés pour mener à bien le projet :

- [ ] Publication du Projet Stategra 
https://www.nature.com/articles/s41597-019-0202-7

- [ ] Bases de données : Cistrome http://cistrome.org/db/#/ génomes pré-B et B-lymphocytes - MGI https://www.informatics.jax.org/allele/MGI:5617014 - ENSEMBL https://www.ensembl.org/Mus_musculus/Gene/Summary?g=ENSMUSG00000018654;r=11:11634980-11722926 

- [ ] Correspondances entre ID des bases de données : https://hgdownload.soe.ucsc.edu/goldenPath/mm39/bigZips/ , fichier mm39.chromAlias.txt

- [ ] Genome de référence GRCm39 : https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_000001635.27/ 
```
$ mkdir GRCm39_annotation-genome_RefSeq
$ curl -o datasets.zip "https://api.ncbi.nlm.nih.gov/datasets/v2alpha/genome/accession/GCF_000001635.27/download?include_annotation_type=GENOME_FASTA,GENOME_GFF,RNA_FASTA,CDS_FASTA,PROT_FASTA,SEQUENCE_REPORT"
$ unzip datasets.zip
$ scp -r GRCm39_annotation-genome_RefSeq/ ubuntu@193.49.167.17:/home/data_local/references
```

# Contribution

Merci à Mme.Gisèle BRONNER et à M.Franck COURT pour leur aide tout au long de ce projet, ainsi que les nombreuses réponses et les éclaircissements fournis pendant cette UE d'Omique Intégrative.