# Importation des bibliothèques
# Importe les bibliothèques nécessaires, notamment TensorFlow pour la création et l'entraînement du modèle, ainsi que d'autres modules pour la gestion des données et des métriques.

import os
import datetime
import numpy as np
from skimage.io import imshow
from matplotlib import pyplot as plt

import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import MeanIoU
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.callbacks import EarlyStopping

import model
import metrics_and_losses

# Définition des chemins des données
# Spécifie les chemins vers les répertoires contenant les images d'entraînement et de validation, ainsi que les masques associés.

DATA_PATH = 'C:/Users/lasarry.UCA/Documents/UE3.8'
TRAIN_FRAME_PATH = os.path.join(DATA_PATH, 'train_frames')
VAL_FRAME_PATH = os.path.join(DATA_PATH, 'val_frames')
TRAIN_MASK_PATH = os.path.join(DATA_PATH, 'train_masks')
VAL_MASK_PATH = os.path.join(DATA_PATH, 'val_masks')

##########################################
# Partie génération et augmentation d'images #
##########################################

# Génération et augmentation d'images
# Utilise «ImageDataGenerator» pour créer des instances de générateurs d'images d'entraînement et de validation.
# L'augmentation géométrique est appliquée uniquement aux images d'entraînement, avec des transformations telles que le décalage, le zoom aléatoire entre 0.8x et 1.2x et la rotation entre 0° et 10°.
# Normalise les valeurs des pixels des images entre 0 et 1.

# Instances de ImageDataGenerator pour les ensembles d'entraînement et de validation
# L'augmentation des données géométriques est appliquée uniquement à l'ensemble d'entraînement
# Les niveaux de gris des images sont normalisés entre 0 et 1
# Sorties : train_datagen et val_datagen
train_datagen = ImageDataGenerator(
    width_shift_range=0.2,
    height_shift_range=0.2,
    zoom_range=0.2,
    rescale=1./255,
    rotation_range=10
)

val_datagen = ImageDataGenerator(
    rescale=1./255
)

# Générateurs d'images et de masques
# Crée des générateurs d'images et de masques à partir des répertoires d'entraînement et de validation en utilisant «flow_from_directory».
# Les générateurs sont configurés pour générer des lots d'images en niveaux de gris avec les mêmes transformations que celles appliquées pendant l'entraînement.

# Construit des générateurs d'images pour les ensembles d'entraînement et de validation
# en utilisant la sous-routine Keras «flow_from_directory»
BATCH_SIZE = 64  # Puissance de deux : 4/8/16/32/64

# Les images doivent être situées dans un sous-répertoire de TRAIN_FRAME_PATH, par exemple /Train
# Sortie : train_image_generator (images de TRAIN_FRAME_PATH)
train_image_generator = train_datagen.flow_from_directory(os.path.join(TRAIN_FRAME_PATH, 'Train'),
    batch_size=BATCH_SIZE, color_mode='grayscale', class_mode=None, seed=42)

# Les images doivent être situées dans un sous-répertoire de TRAIN_MASK_PATH, par exemple /Train
# Utilisez la même graine que les images d'entraînement pour que l'augmentation soit la même
# Sortie : train_mask_generator (images de TRAIN_MASK_PATH)
train_mask_generator = train_datagen.flow_from_directory(TRAIN_MASK_PATH,
    batch_size=BATCH_SIZE, color_mode='grayscale', class_mode=None, seed=42)

# Les images doivent être situées dans un sous-répertoire de VAL_FRAME_PATH, par exemple /Val
# Sortie : val_image_generator (images de VAL_FRAME_PATH)
val_image_generator = val_datagen.flow_from_directory(VAL_FRAME_PATH,
    batch_size=BATCH_SIZE, color_mode='grayscale', class_mode=None, seed=63)

# Les images doivent être situées dans un sous-répertoire de VAL_MASK_PATH, par exemple /Val
# Utilisez la même graine que les images de validation pour que l'augmentation soit la même
# Sortie : val_mask_generator (images de VAL_MASK_PATH)
val_mask_generator = val_datagen.flow_from_directory(VAL_MASK_PATH,
    batch_size=BATCH_SIZE, color_mode='grayscale', class_mode=None, seed=63)

train_generator = zip(train_image_generator, train_mask_generator)
val_generator = zip(val_image_generator, val_mask_generator)

# Affichage des images générées
# Affiche visuellement une image générée pour chaque générateur (entraînement et validation) pour vérifier les effets de l'augmentation.

# Affiche la première image générée pour les quatre générateurs d'images
img = np.squeeze(train_image_generator.next()[0])
imshow(img)
plt.show()
img = np.squeeze(train_mask_generator.next()[0])
imshow(img)
plt.show()
img = np.squeeze(val_image_generator.next()[0])
imshow(img)
plt.show()
img = np.squeeze(val_mask_generator.next()[0])
imshow(img)
plt.show()

#################
# Partie Entraînement #
#################

# Entraînement du modèle
# Spécifie le nombre d'images d'entraînement et de validation.
# Charge le modèle U-Net en utilisant la fonction «get_UNet» du module «model».
# Compile le modèle avec l'optimiseur Adam, la fonction de perte binaire (cross-entropy), et la métrique de coefficient de Dice.
# L'optimiseur permet l'optimisation des attributs du réseau de neurones, tels que le taux d'apprentissage et le poids, en minimisant la fonction de perte.
# On a utilisé Adam, une extension de l'algorithme de descente de gradient, avec un temps de calcul rapide et moins de paramétrage.
# Permet la mise à jour des poids de manière itérative en fonction des données d'apprentissage.
# Configure les callbacks pour sauvegarder les meilleurs poids, enregistrer les logs, et arrêter l'entraînement de manière anticipée si la performance ne s'améliore pas.

NO_OF_TRAINING_IMAGES = len(os.listdir(os.path.join(TRAIN_FRAME_PATH, 'Train')))
NO_OF_VAL_IMAGES = len(os.listdir(os.path.join(VAL_FRAME_PATH, 'Val')))

NO_OF_EPOCHS = 100  # Entre 30 et 100 pour des ensembles de données petits ou moy

ens

# Charge le modèle CNN
m = model.get_UNet(256, 256)
m.summary()

# Optimiseur
opt = Adam(learning_rate=1E-5, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

# Compile le modèle avec l'optimiseur, la fonction de perte choisie (cross-entropy binaire) et les métriques de surveillance
# Les choix pour la fonction de perte sont le coefficient de Dice ou la cross-entropy binaire
m.compile(optimizer=opt, loss='binary_crossentropy', metrics=[metrics_and_losses.dice_coef])

# Paramètres pour l'optimisation
# Les poids du modèle sont sauvegardés dans weights.h5 lorsque la perte de validation est améliorée
# La perte et les métriques pour les ensembles d'entraînement et de validation sont stockées dans log.out à chaque époque
# L'optimisation s'arrête s'il n'y a aucune amélioration de la perte de validation pendant au moins 10 époques
weights_path = './weights.h5'
checkpoint = ModelCheckpoint(weights_path, verbose=1, save_best_only=True)
csv_logger = CSVLogger('./log.out', append=True, separator=';')
earlystopping = EarlyStopping(verbose=1, patience=10)
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
callbacks_list = [checkpoint, csv_logger, earlystopping, tensorboard_callback]

# Utilise «fit_generator» pour entraîner le modèle avec les générateurs d'images et de masques.
# L'entraînement du modèle avec la fonction fit(), en utilisant les données d'apprentissage et de validation construites précédemment avec les générateurs.
# On précise le nombre d'itérations (époques), étape par époque, et on appelle la liste contenant toutes les variables de sortie construites précédemment.
# En effet, pour steps_per_epoch, pour chaque époque, un nombre différent de lots est utilisé jusqu'à ce que l'on couvre la base de données.
# On termine cette étape par la sauvegarde des poids du réseau courant avec la fonction ’Save_Weights’.
# Les résultats de l'entraînement, tels que les pertes et les métriques, sont stockés dans la variable «results».

# Entraînement du modèle m en utilisant les générateurs précédemment définis
results = m.fit_generator(train_generator, epochs=NO_OF_EPOCHS, steps_per_epoch=NO_OF_TRAINING_IMAGES//BATCH_SIZE, validation_data=val_generator, validation_steps=NO_OF_VAL_IMAGES//BATCH_SIZE, callbacks=callbacks_list)

# Sauvegarde les poids du modèle après l'entraînement dans un fichier appelé 'Final_weights.h5'.
m.save_weights('Final_weights.h5')