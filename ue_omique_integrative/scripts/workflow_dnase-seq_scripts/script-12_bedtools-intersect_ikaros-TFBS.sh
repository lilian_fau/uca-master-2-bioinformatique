#!/bin/bash
#
# bedtools version v2.31.0 
#
# perform intersection of peakcalling file and ikaros TFBS regions


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/consensus_dhs/"
OUTPUT="$WKDIR/intersect_data_B_preB_ikzf1"
mkdir -p -m 770 $OUTPUT

dhs="${INPUT}/all_consensus_dhs.bed"
# ikzf1 tfbs sites in B_Lymphocyte
ikaros_B="$WKDIR/references/Ikaros_binding_sites/B_Lymphocyte_mm39_IKZF1_peaks.bed"
ikaros_preB="$WKDIR/references/Ikaros_binding_sites/preB_mm39_IKZF1_peaks.bed"

ikaros="$WKDIR/references/Ikaros_binding_sites/preB_B_Lymphocyte_mm39_IKZF1_peaks.bed"
outfile1="${OUTPUT}/DHS_with_ikzf1_tfbs.bed"
outfile2="${OUTPUT}/ikzf1_tfbs_in_DHS.bed"
outfile3="${OUTPUT}/ikzf1_tfbs_in_DHS_DHS-id-peack.bed"
outfile4="${OUTPUT}/DHS_and_ikzf1-tfbs_positions.tab"

#--------------------------------------------------------------------------------------------------------------------------------

# B-lymphocyte and preB-lymphocyte ikzf1 tfbs merging db
cat <(sed 's/peak/B_peak/' "$ikaros_B") <(sed 's/peak/preB_peak/' "$ikaros_preB") | sort -k2 > "$ikaros"
#cat <(sed 's/peak/B_peak/' "$ikaros_B") <(sed 's/peak/preB_peak/' "$ikaros_preB") | awk '{print $4, $1, $2, $3}' | sort -k2 | awk '{print $2, $3, $4, $1}' | uniq -f > "$ikaros"



# get DHS with ikzf1 tfbs

bedtools intersect -a "$dhs" -b "$ikaros" | sort-bed - > "$outfile1"

# get ikzf1 positions in consensus DHS regions

bedtools intersect -a "$ikaros" -b "$dhs" | sort-bed - > "$outfile2"

paste "$outfile2" <(cat "$outfile1" | cut -f4) > "$outfile3"

# make tab file with fields "DHS chr start end tfbs_peak start end"

echo -e "DHS\tchr\tstart\tend\ttfbs_peak\tstart\tend" > "$outfile4"
join -1 4 <(cat "$dhs" | sort -k4) -2 6 <(cat "$outfile3" | sort -k6) | awk -v FS=" " -v OFS="\t" '{print $1, $2, $3, $4, $8, $6, $7}' >> "$outfile4"

echo file created :
# List the created output files
ls "${outfile1}"
ls "${outfile2}"
ls "${outfile3}"
ls "${outfile4}"
