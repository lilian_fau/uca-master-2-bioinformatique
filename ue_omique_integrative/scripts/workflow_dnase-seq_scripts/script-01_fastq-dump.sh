#!/bin/bash
#
# fastq-dump version 3.0.8

echo
echo $0 # print script name
echo 


#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/raw_data"
OUTPUT="$WKDIR/raw_data"

CONDITION1="18h"
CONDITION2="control"

#--------------------------------------------------------------------------------------------------------------------------------

# Metadata file path
metadata=$INPUT/Metadata_PRJNA304160_T0_T18.txt

# Convert commas to semicolons in metadata and save as CSV
cat ${metadata} | sed 's/,/;/g' > $OUTPUT/Metadata_PRJNA304160_T0_T18.csv

# Update metadata variable
metadata=$OUTPUT/Metadata_PRJNA304160_T0_T18.csv

# Extract relevant SRA IDs based on specific conditions
sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${CONDITION1};${CONDITION2}$" | cut -d';' -f1)

# Loop through SRA IDs
for srr in ${sraIds}; do
	echo "_____________ start ${srr} _____________\n"

	# Measure execution time
	SECONDS=0

	echo "Check for empty dir..."
	# Check if the output directory exists and is not empty
	if [ -d "${OUTPUT}" ]; then
		if [ "$(ls -A ${OUTPUT}/${srr})" ]; then
			echo "in ${OUTPUT}"
			echo "${srr}" is not empty and will be cleared
			#rm -rf "${OUTPUT}/${srr}"
		else
			echo "in ${OUTPUT}"
			echo "${srr}" is empty
		fi
	fi
	
	# Create output directory
	mkdir -p -m 770 ${OUTPUT}/${srr}

	echo "run fastq-dump..."
	# Run fastq-dump to download and process FASTQ files
    fastq-dump ${srr} --OUTPUT ${OUTPUT}/${srr} --split-3 --gzip

	echo "files downloaded :"
	# List downloaded files
    ls ${OUTPUT}/${srr}/*fastq.gz

    # Set file permissions
    chmod 440 ${OUTPUT}/${srr}/*fastq.gz 

	echo -e "_____________ end ${srr} _____________"
	echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
	echo
done