# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 29/02/24
# Script to retrieve PTM scores of the best models predicted by colabfold

# LIBRARIES--------------------------------------------------------------------------------------

import os
import re
import json
import csv

# CONFIG------------------------------------------------------------------------------------------

# Set the working directory and paths to necessary folders and files
wk_directory = "/home/lilian/stage"
json_folder = f"{wk_directory}/datas/monomers/results/EFF_no_p_signal_structures_results"
output_csv_path = f"{wk_directory}/datas/monomers/results/EFF_no_p_signal_structures_results/output_EFF_ptm.csv"

# CODE--------------------------------------------------------------------------------------------

# List to store the names of JSON files of the best models
json_files = []

# Detection of JSON files of the best models
for file_name in os.listdir(json_folder):
    if re.search(r"rank_001.*\.json", file_name):
        json_files.append(file_name)

# Dictionary to store PTM scores
ptm_scores = {}

# Iterating through JSON files of the best models
for json_file in json_files:
    with open(f"{json_folder}/{json_file}", 'r') as file:
        data = json.load(file)

    # Retrieve the value of "ptm" from the JSON dictionary
    if "ptm" in data:
        ptm_value = data["ptm"]

        # Split the file name and use the first element as the key
        key_name = json_file.split("_")[0]
        ptm_scores[key_name] = ptm_value

# Writing to the CSV file
with open(output_csv_path, 'w', newline='') as csvfile:
    fieldnames = ['protein', 'PTM']  # Corrected field names
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for key, value in ptm_scores.items():
        writer.writerow({'protein': key, 'PTM': value})  # Corrected field names

# Displaying a message indicating that the CSV file has been successfully created
print(f"The CSV file has been successfully created: {output_csv_path}")
