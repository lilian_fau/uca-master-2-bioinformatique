#!/bin/bash
#
# bedops version 2.4.41
# bedtools version v2.31.1
#
# perform merge of peakcalling file 


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/peak_data"
OUTPUT="$WKDIR/consensus_dhs"
mkdir -p -m 770 "$OUTPUT"

#--------------------------------------------------------------------------------------------------------------------------------

peaks_data=($(find $INPUT -maxdepth 1 -type f -name "*dhs.bed" | sort))

echo "peaks_data="
printf "%s\n" "${peaks_data[@]}"
echo

outfile1="$OUTPUT/all_consensus_dhs.bed"

# merge all opened DNA regions containning tfbs ikzf1 of all samples
bedops -m "${peaks_data[@]}" | bedtools sort -g <(echo -e "chr1\nchr2\nchr3\nchr4\nchr5\nchr6\nchr7\nchr8\nchr9\nchr10\nchr11\nchr12\nchr13\nchr14\nchr15\nchr16\nchr17\nchr18\nchr19\nchr20\nchr21\nchr22\nchrX\nchrY") -i - > "${outfile1}.merge"

echo "Analysis completed !"

# sort peaks and add peak name

chr_names="/home/data_local/references/GRCm39_annotation-genome_RefSeq/mm39.chromAlias.txt"

join -1 1 <(sort -k1,1 "$chr_names") -2 1 <(sort -k1,2 "${outfile1}.merge") | tr " " "\t" | cut -f1,5- | awk -v OFS="\t" ' {print $0, "DHS_"NR}' | cut -f1,3- > "${outfile1}"

rm -f "${outfile1}.merge"

# Get consensus DH by conditions

for bed in "${peaks_data[@]}"
do
	outbed=${OUTPUT}/$(basename "$bed" "_dhs.bed")_consensus_dhs.bed
	bedtools intersect -a "${outfile1}" -b "$bed" > "$outbed"
done

# consensus DHS output file
echo "consensus DHS :"
ls -1 "$OUTPUT"/*
