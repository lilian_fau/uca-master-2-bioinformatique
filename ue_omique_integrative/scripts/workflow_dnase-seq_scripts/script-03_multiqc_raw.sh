#!/bin/bash
#
# multiqc version 1.17
#
# Multi Quality control report on raw datas with multiqc

echo
echo $0 
echo 

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/raw_data"
OUTPUT="$WKDIR/raw_data"

CONDITION1="18h"
CONDITION2="control"

#--------------------------------------------------------------------------------------------------------------------------------

# Set the path for the metadata file in CSV format
metadata=$INPUT/Metadata_PRJNA304160_T0_T18.csv

# Extract specific fields from metadata based on conditions and filter by "DNase-Hypersensitivity;18h;control$"
sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${CONDITION1};${CONDITION2}$" | cut -d';' -f1)

# Loop through the list of SRA IDs
for srr in ${sraIds}; do
	echo "_____________ start ${srr} _____________\n"

        # Measure execution time
        SECONDS=0
	
        # Define the report name for Multiqc
        report_name="${srr}_multiqc-raw"

        echo "removing old Multiqc reports..."
        # Remove previous Multiqc reports for the current SRA ID
        #rm -f ${OUTPUT}/${srr}/${report_name}*

        echo "following reports will be used :"
        # List the FastQC reports that will be used by Multiqc
        ls ${OUTPUT}/${srr}/*_fastqc.zip

        echo "run Multiqc on raw QC..."
        # Run Multiqc on the FastQC reports to generate a summary report
        multiqc ${OUTPUT}/${srr}/*_fastqc.zip \
                --outdir ${OUTPUT}/${srr} \
                --force \
                --filename ${report_name} \
                --zip-data-dir

        echo "files created :"
        # List the Multiqc report files created
        ls ${OUTPUT}/${srr}/${report_name}*

        echo -e "_____________ end ${srr} _____________"
        echo nb threads : "${nbThreads}"
        echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
        echo

done
