# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 19/02/24

# Basic script to retrieve each sequence from a fasta file (multiple sequences) and create its own file.
# Script used to format the 31 effector sequences for processing in ColabFold batch.

# CONFIG------------------------------------------------------------------------------------------

# Name of the input file
input_file = "summary_sequences.fasta"

# Separator used in the file
separator = ">"

# Initialize the counter for the number of IDs
id_count = 0

# CODE--------------------------------------------------------------------------------------------

# Open the input file and read its content
with open(input_file, 'r') as file:
    # Iterate through each line in the file
    for line in file:
        # Increment the ID count for each line
        id_count += 1

        # Check if the line starts with ">"
        if line.startswith(separator):
            # Extract the file name and sequence from the line
            parts = line[1:].strip().split()
            file_name = parts[0]
            sequence = parts[1]

            # Create a new file with the extracted name
            output_file = f"{file_name}.fasta"

            # Write the sequence to the output file
            with open(output_file, 'w') as output:
                output.write(f">{file_name}\n{sequence}")

# Print the total number of IDs processed
print(f"Total number of IDs processed: {id_count}")
