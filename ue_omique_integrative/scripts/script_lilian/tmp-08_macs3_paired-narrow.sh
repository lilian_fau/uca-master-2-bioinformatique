#!/bin/bash
#
# macs3 version 3.0.0a6
#
# peakcalling independtly on each replicate
#

echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/mapping_data"
OUTPUT="$WKDIR/peak_data"

#--------------------------------------------------------------------------------------------------------------------------------

# Measure execution time
SECONDS=0


echo "Check for empty dir..."
if [ -d "${OUTPUT}" ]
then
	if [ "$(ls -A ${OUTPUT}/ 2> /dev/null)" ]
	then
		echo "${OUTPUT} is not empty, removing hold files"
                echo "noting removed !!!"
				#rm -f ${OUTPUT}
	else
		echo "${OUTPUT}" is empty
	fi
fi

# Create the output directory
mkdir -p -m 770 ${OUTPUT}

# Find input files (BAM files) for peak calling
infiles=($(find ${INPUT} -type f -name "SRR*map_trim_paired.bam"))

# Print the list of input files
echo "following files will be used for peak-calling :"
echo "${infiles[@]}" | tr " " "\n"

echo "--------------------------------------------------------------"

for bam in ${infiles[@]}
do
	echo "run macs3 callpeak on ${bam/*\/} ..."
# Run MACS3 peak calling with specified parameters
	outfile=$(basename "$bam" "_map_trim_paired.bam")

	macs3 callpeak \
		--min-length 150 \
		-f BAMPE \
		--treatment "$bam" \
		--gsize 'mm' \
		--outdir "$OUTPUT" \
		--name "$outfile"

	echo "files created :"
	ls ${OUTPUT}/${outfile}*
	echo
done

echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec


