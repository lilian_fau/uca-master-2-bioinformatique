#!/bin/bash
#
# set and make output directory for control_t0 trimmed DNas-seq datas
#
# usage : give as first argument the number of thread to be used

echo "usage : give as first argument the number of thread to be used"
echo
echo -e "\t${0/*\/} nbThreads"

nbThreads=${1:-4}

echo
echo $0
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/raw_data"
OUTPUT="$WKDIR/trim_data"

CONDITION1="18h"
CONDITION2="control"

#--------------------------------------------------------------------------------------------------------------------------------

# Set the path for the metadata file in CSV format
metadata=$INPUT/Metadata_PRJNA304160_T0_T18.csv

# Extract specific fields from metadata based on conditions and filter by "DNase-Hypersensitivity;18h;control$"
sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${CONDITION1};${CONDITION2}$" | cut -d';' -f1)

# Set the path for TruSeq3-PE adapters
ADAPTERS="$OUTPUT/TruSeq3-PE.fa"

# Download TruSeq3-PE adapters if not already present
if [ ! -f "$ADAPTERS" ]; then
    wget https://raw.githubusercontent.com/usadellab/Trimmomatic/main/adapters/TruSeq3-PE.fa -O "$ADAPTERS"
fi

# Loop through the list of SRA IDs
for srr in ${sraIds}; do
    echo "_____________ start ${srr} _____________"

    # Measure execution time
    SECONDS=0

    echo "Check for an empty directory..."
    # Check if the output directory exists
    if [ -d "${OUTPUT}" ]; then
        # Check if the specific SRA ID directory is not empty
        if [ "$(ls -A ${OUTPUT}/${srr})" ]; then
            echo "in ${OUTPUT}"
            echo "${srr}" is not empty and will be cleared
            #rm -rf "${OUTPUT}/${srr}"
        else
            echo "in ${OUTPUT}"
            echo "${srr}" is empty
        fi
    fi

    # Create output directory with specific permissions
    mkdir -p -m 770 ${OUTPUT}/${srr} 

    # Set file paths
    f1=${INPUT}/${srr}/${srr}_1.fastq.gz # forward reads file
    f2=${INPUT}/${srr}/${srr}_2.fastq.gz # reverse reads file

    # Set log and summary file paths
    log_file=${OUTPUT}/${srr}/${srr}_trim.log # log file
    sum_file=${OUTPUT}/${srr}/${srr}_trim.sum # summary
    out=${OUTPUT}/${srr}/${srr}_trimmed.fastq.gz # file base for trimmed reads files

    echo "run trimming on raw data..."
    
    # Run Trimmomatic for trimming
    trimmomatic PE -threads ${nbThreads} \
        -trimlog ${log_file} \
        -summary ${sum_file} \
        ${f1} ${f2} \
        -baseout ${out} \
        ILLUMINACLIP:${ADAPTERS}:2:30:10 \
        LEADING:5 TRAILING:5 SLIDINGWINDOW:4:5 MINLEN:25

    # Compress the log file
    gzip ${log_file}

    echo "files created :"
    # List files in the output directory
    ls ${OUTPUT}/${srr}

    echo -e "_____________ end ${srr} _____________"
    echo nb threads : "${nbThreads}"
    echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
    echo

    # Set permissions for the created files
    chmod 440 ${OUTPUT}/${srr}/* 
done
