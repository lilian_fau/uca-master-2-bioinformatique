import os

def read_fasta(file_path):
    sequences = {}
    with open(file_path, 'r') as file:
        lines = file.readlines()
        current_id = None
        current_sequence = ""
        for line in lines:
            if line.startswith('>'):
                if current_id:
                    sequences[current_id] = current_sequence
                current_id = line[1:].strip()
                current_sequence = ""
            else:
                current_sequence += line.strip()
        if current_id:
            sequences[current_id] = current_sequence
    return sequences

def write_fasta(file_path, sequence_id, sequence):
    with open(file_path, 'w') as file:
        file.write(f">{sequence_id}\n{sequence}\n")

def generate_combinations(sequences, output_folder):
    sequence_ids = list(sequences.keys())
    for i in range(len(sequence_ids)):
        for j in range(i, len(sequence_ids)):  # Ajustement pour inclure les combinaisons avec le même
            seq1 = sequence_ids[i]
            seq2 = sequence_ids[j]
            
            combined_sequence = f"{sequences[seq1]}:{sequences[seq2]}"
            output_file_path = f"{output_folder}/{seq1}_x_{seq2}.fasta"
            write_fasta(output_file_path, f"{seq1}_x_{seq2}", combined_sequence)

def main(input_file, output_folder):
    sequences = read_fasta(input_file)
    output_folder = f"{output_folder}/TRB_input"
    
    # Créer le dossier de sortie s'il n'existe pas
    os.makedirs(output_folder, exist_ok=True)
    
    generate_combinations(sequences, output_folder)

if __name__ == "__main__":
    input_file = "/home/lilian/stage/datas/Multimers/TRB.fasta"
    output_folder = "/home/lilian/stage/datas/Multimers"
    main(input_file, output_folder)
