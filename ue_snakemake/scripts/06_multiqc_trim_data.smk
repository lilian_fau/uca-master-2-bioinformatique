#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 1 --snakefile 06_multiqc_trim_data.smk
# This sixth script merges the DNAse-seq cleaned data quality control files with the multiqc tool, in order to obtain a better view of the data set after cleaning.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        "../results/multiqc_trim.html"

rule multiqc_raw:
    input:
        fastqc_zip=expand("../data/3.fastqc_trimming/trimmed_{sample}_P_fastqc.zip", sample=config["samples"]),
    output:
        html="../results/multiqc_trim.html" 
    resources:
        mem_mb=5,  
        time="00:10:00"
    #log: "../log/06_multiqc_trim_data/multiqc.log"
    shell: 
        """
        ml gcc/8.1.0 python/3.7.1 MultiQC/1.7
        multiqc {input.fastqc_zip} -n multiqc_trim -o ../results/
        """