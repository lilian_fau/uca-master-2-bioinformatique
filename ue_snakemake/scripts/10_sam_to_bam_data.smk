#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 2 --snakefile 10_sam_to_bam_data.smk
# This tenth script converts the sam files into bam files using the Picard tool, both to save space and to generate the counting matrix. 
# The sam files are deleted once this script has been run.
# This step is heavy, so be careful if you increase the command-c 2, the java memory may cause the script to crash.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/4.mapping/sorted_bam/sorted_{sample}.bam", sample=config["samples_compiled"]),

rule create_output_directory:
    shell:
      """
      mkdir ../data/4.mapping/sorted_bam
      """

rule sort_and_convert:
    input: 
      "../data/4.mapping/trimmed_{sample}_mapping.sam"
    output: 
      "../data/4.mapping/sorted_bam/sorted_{sample}.bam"
    resources:
        mem_mb=5000,  
        time="00:10:00"
    #log: "../log/10_sam_to_bam_data/{sample}.log"
    shell:
        """
        ml java
        ml picard/2.18.25
        java -jar /opt/apps/picard-2.18.25/picard.jar SortSam \
          I={input} \
          O=/dev/stdout \
          SORT_ORDER=coordinate | \
          java -jar /opt/apps/picard-2.18.25/picard.jar SamFormatConverter \
            I=/dev/stdin \
            O={output}
        """
