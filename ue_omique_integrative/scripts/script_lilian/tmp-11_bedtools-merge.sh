echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/peak_data"
OUTPUT="$WKDIR/peak_data"

#--------------------------------------------------------------------------------------------------------------------------------


peaks_data=$(find $INPUT -type f -name "*IKZF1.bed")
outfile="$OUTPUT/SRR_all_peaks_merge.bed"

bedops -u $peaks_data | bedtools sort -g <(echo -e "chr1\nchr2\nchr3\nchr4\nchr5\nchr6\nchr7\nchr8\nchr9\nchr10\nchr11\nchr12\nchr13\nchr14\nchr15\nchr16\nchr17\nchr18\nchr19\nchr20\nchr21\nchr22\nchrX\nchrY") -i - > $outfile

echo "Analysis complete."

echo file created :
# List the created output file
ls "${outfile}"
