#!/bin/bash

# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 24/04/24

# This script is a pipeline for performing homologue search, multiple sequence alignment, and phylogenetic tree construction on a set of databases. The results are written to files in the specified output directory.

# 1. Parse Command Line Arguments: The script accepts three command line arguments: `--fasta` for the FASTA file, `--db` for the database directory, and `--output` for the results directory.
# 2. Preprocessing: The script checks if the required arguments are set and converts the FASTA file to Unix format.
# 3. Database Processing: The script iterates over each database file in the specified directory and performs a reciprocal best hit (RBH) analysis using `mmseqs2`. The results are sorted, duplicates are removed, and the unique RBHs are written to a file.
# 4. Sequence Extraction: The script extracts the sequences corresponding to the unique RBHs from the database files and writes them to a file. The sequences are written to files in the `01.sequences` directory.
# 5. Multiple Sequence Alignment: The script groups the sequences by ID and performs a multiple sequence alignment using `mafft` for each group. The alignments are written to files in the `02.alignments` directory.
# 6. Phylogenetic Tree Construction: The script constructs a phylogenetic tree for each alignment using `iqtree`. The trees are written to files in the `03.trees` directory.
# 7. Common Dataset Identification: The script identifies the common dataset by finding the common IDs between the results of each database.
# 8. Consensus Tree Construction: The script generates a consensus tree from the common dataset using `iqtree`.

# To optimize: Section 3./4. and 6. which could be parallelized to obtain a significant saving in time during execution

# REQUIREMENTS--------------------------------------------------------------------------------------

# The databases used by the script must be created using the mmseqs createdb command. 

# DEPENDENCIES--------------------------------------------------------------------------------------

# LINUX/BASH (or WSL2 [Windows Subsystem Linux] under Windows 10/11)

# dos2unix - v7.5.2 [conda install conda-forge::dos2unix=7.5.2]
# iqtree - v2.3.0 [conda install bioconda::iqtree=2.3.0]
# mafft - v7.525 [conda install bioconda::mafft=7.525]
# mmseqs2 - v15.6f452 [conda install bioconda::mmseqs2=15.6f452]


# 1.COMMAND LINE------------------------------------------------------------------------------------

# The script accepts three command line arguments: `--fasta` for the FASTA file, `--db` for the database directory, and `--output` for the results directory.


# Define default values for arguments
FASTA_FILE=""
DB_DIR=""
RESULTS_DIR=""

# Parse command line arguments using getopts
while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    --fasta)
    FASTA_FILE="$2"
    shift # past argument
    shift # past value
    ;;
    --db)
    DB_DIR="$2"
    shift # past argument
    shift # past value
    ;;
    --output)
    RESULTS_DIR="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    echo "Unknown option: $key"
    exit 1
    ;;
  esac
done


# 2.PREPROCESSING-----------------------------------------------------------------------------------

# The script checks if the required arguments are set and converts the FASTA file to Unix format.


# Check if required arguments are set
if [ -z "${FASTA_FILE}" ] || [ -z "${DB_DIR}" ] || [ -z "${RESULTS_DIR}" ]; then
  echo "Usage: $0 [--fasta <FASTA_FILE>] [--db <DB_DIR>] [--output <RESULTS_DIR>]"
  exit 1
fi

# Fix mmseqs2 : Input database "$FASTA_FILE" has the wrong type (Generic)
dos2unix --quiet "${FASTA_FILE}"


# 3.DATABASE PROCESSING-----------------------------------------------------------------------------

# The script iterates over each database file in the specified directory and performs a reciprocal best hit (RBH) analysis using `mmseqs2`. 
# The results are sorted, duplicates are removed, and the unique RBHs are written to a file.


# Define the new directory containing the database files
DB_SEQ_DIR="${DB_DIR}"

# Fix excess backslash in variable
RESULTS_DIR=$(echo "${RESULTS_DIR}" | sed 's/\/\([^/]*\)$/\1/')

# Create a unique results folder with the name of the fasta file used +"_results". 
RESULTS_DIR="${RESULTS_DIR}/$(basename "${FASTA_FILE%.*}")_results"

# Create the 'results' directory if it doesn't already exist
SEQUENCES_DIR="${RESULTS_DIR}/01.sequences"

mkdir -p "${SEQUENCES_DIR}"

# Create log file name with the name of the script and the current date and time
LOG_FILE="${RESULTS_DIR}/$(basename "$0")_$(date '+%Y-%m-%d_%H-%M-%S').log"

# Redirect stdout and stderr to the log file and display them on the console using tee
exec > >(tee -a "${LOG_FILE}") 2>&1

echo -e "\n################################### Homologue Search #########################################\n"

# Find all .fa files in the directory and store them in an array
DB_FILES=( "${DB_SEQ_DIR}"*.fa )
DB_count=${#DB_FILES[@]}

echo "Databases detected: ${DB_count}"

# Initialize counter
i=0

# Iterate over each .fa file
for db_file in "${DB_FILES[@]}"; do
    i=$((i+1))
    echo -e "\n----------------------------------------------------------------------------------------------\n"
    echo -e "Processing database [${i}/${DB_count}]: ${db_file}"

    # Create a result file name for each database
    filename=$(basename "${db_file}")

    # Extract the name of database file without extension
    species=$(basename "${db_file}" .fa)

    # Check if the output file exists and is not empty
    output_file="${SEQUENCES_DIR}/tmp_rbh_${filename}"
    if [ -s "${output_file}" ]; then
        echo -e "Processing existing output file: ${output_file} \t >>> \t OK"
    else
        # If the file is empty or does not exist, run mmseqs
        echo -e "\n  >>> mmseqs2 easy-rbh analysis, please wait"
        mmseqs easy-rbh "${FASTA_FILE}" "${db_file}" "${output_file}" "${RESULTS_DIR}/tmp" > /dev/null || { echo -e "\nmmseqs failed with error" >&2; exit 1; }
    fi

    # Sort and remove duplicates in each result file
    results=$(sort -u -k1,1 "${output_file}" | awk -v species="${species}" '{print species "\t" $0}')

    #Number of results
    number_results=$(echo "${results}" | wc -l)

    # Display the results
    echo
    echo "Reciprocal Best Hits results for ${filename} - ${number_results} RBH:"
    echo "${results}"
    echo


# 4.SEQUENCES EXTRACTION----------------------------------------------------------------------------

# The script extracts the sequences corresponding to the unique RBHs from the database files and writes them to a file. 
# The sequences are written to files in the `01.sequences` directory.


    # Check if the sequences file exists and is not empty
    sequences_file="${SEQUENCES_DIR}/rbh_${species}_sequences.fa"
    
    if [ -s "${sequences_file}" ]; then
        echo -e "Processing existing sequences file: ${sequences_file} \t >>> \t OK"
    else
        # If the file is empty or does not exist, write the header and results to the file

        echo -ne "Extracting sequences from the database file: ${db_file}"
        echo "Selection of unique Reciprocal Best Hits - ${number_results} RBH:" >> "${sequences_file}"
        echo "-----------------------------------------------------------------------------------------" >> "${sequences_file}"
        echo "$results" >> "${sequences_file}"
        echo -e "-----------------------------------------------------------------------------------------\n" >> "${sequences_file}"
        
        
        # Read the FASTA file once and store sequences in a variable
        sequences=$(awk '/^>/ {if (seq) print seq; seq=""; printf "%s\n", $0; next} {seq = seq""$0} END {print seq}' "${db_file}")

        # Sort and remove duplicates in each result file
        sort -u -k1,1 "${output_file}" | cut -f2 | while read -r ID; do
            # Search for the ID in the linearized database file and retrieve the corresponding line and the next one
            sequence=$(echo "${sequences}" | awk -v id="${ID}" '$0 ~ id {print; getline; print}')

            # New header for selected sequences
            new_header=$(echo "${ID}" | awk -v species="${species}" '{print $0 "_" species}')

            # Replace the header with the new header and write to the file
            echo "${sequence}" | awk -v new_header="${new_header}" 'BEGIN{f=0}/^>/{$0=">"new_header; f=1} {print} END{if(!f) print ">"new_header}' >> "${SEQUENCES_DIR}/rbh_${species}"_sequences.fa

done
echo -e "\t >>> \t OK"
echo -e "The sequences have been written to: ${SEQUENCES_DIR}/rbh_${species}_sequences.fa"
fi
done

# Remove temporary directory created by mmseqs
echo -ne "\nRemoving temporary directory created by mmseqs2"
rm -rf "${RESULTS_DIR}/tmp"                                        #commande du Sheitan  ヾ( ･`⌓´･)ﾉﾞ
echo -e "\t >>> \t OK"


# 5.MULTIPLE SEQUENCE ALIGNMENT---------------------------------------------------------------------

# The script groups the sequences by ID and performs a multiple sequence alignment using `mafft` for each group.
# The alignments are written to files in the `alignments` directory.


echo -e "\n################################## Multiple Alignment ########################################\n"

ALIGNMENTS_DIR="${RESULTS_DIR}/02.alignments"

# Check if the alignments directory already exists
if [ -d "$ALIGNMENTS_DIR" ]; then
# Delete all files in the alignments directory
  rm -f "${ALIGNMENTS_DIR}"/*.fa
else
# Create the alignments directory
  mkdir -p "${ALIGNMENTS_DIR}"
fi

# Loop through the rbh_*_sequences.fa files
for result_file in "${SEQUENCES_DIR}"/rbh_*_sequences.fa; do
    # Extract the identifiers from column 2 and their correspondences in column 3
    ids=$(sed -n '/^--*$/,/^--*$/p' "${result_file}" | grep -E '^([A-Za-z0-9].*)$' | awk '{print $2, $3}')

    # Loop through the identifiers and their correspondences
    while read -r id seq_id; do
        # Create a FASTA file for each identifier in column 2
        output_file="${ALIGNMENTS_DIR}/${id}.fa"

        # Extract the FASTA sequences corresponding to the identifier in column 3
        sequence=$(grep -A 1 "^>${seq_id}_" "${result_file}")

        #---------------------------Normalized names for consensus tree---------------------------#

        # Modify the name of the new header in the FASTA file to keep only the species code
        new_sequence_id=$(echo -e "${sequence}" | sed "s/>${seq_id}_/>/")

        # Add the new header to the FASTA sequences
        echo -e "${new_sequence_id}" >> "${output_file}"

        #------------------------------------------------------------------------------------------#

        # Add the sequence to the FASTA file
        #echo "$sequence" >> "$output_file"

    done <<< "${ids}"
done

echo -e "The sequences have been grouped in the directory: ${ALIGNMENTS_DIR}\n"

# Loop through all FASTA files in the alignments directory
for file in "${ALIGNMENTS_DIR}"/*.fa; do
    # Name of the output file (replace the .fa extension with .aln)
    output_file="${file%.fa}.aln"

    # Check if the output file exists and is not empty
    if [ -s "${output_file}" ]; then
        # Remove the alignment file if it exists and is not empty
        rm "${output_file}"
    fi

    # Run MAFFT to create or recreate the alignment
    echo -ne "$(basename "${file}") \t >>> \t mafft multiple alignment"
    mafft --globalpair --maxiterate 16 --phylipout --auto --quiet --inputorder "${file}" > "${output_file}" || exit 1
    # Add an echo to indicate that the iqtree step is done
    echo -e "\t >>> \t OK"
done

echo -e "\nThe alignments have been generated and stored in the directory: ${ALIGNMENTS_DIR}\n"


# 6.PHYLOGENETIC TREE CONSTRUCTION------------------------------------------------------------------

# The script constructs a phylogenetic tree for each alignment using `iqtree`. 
# The trees are written to files in the `tree` directory.


echo -e "#################################### Phylogenetic Tree #######################################\n"

TREES_DIR="${RESULTS_DIR}/03.trees"

# Create a directory to store the trees
mkdir -p "${TREES_DIR}"

# Loop through all alignment files in the alignments directory
for file in "${ALIGNMENTS_DIR}"/*.aln; do
    
    base_filename=$(basename "${file}")

    # Check if all output iqtree files exist
    if [ -e "${TREES_DIR}/${base_filename}.contree" ] &&
       [ -e "${TREES_DIR}/${base_filename}.iqtree" ] &&
       [ -e "${TREES_DIR}/${base_filename}.mldist" ] &&
       [ -e "${TREES_DIR}/${base_filename}.treefile" ] &&
       [ -e "${TREES_DIR}/${base_filename}.bionj" ] &&
       [ -e "${TREES_DIR}/${base_filename}.ckp.gz" ] &&
       [ -e "${TREES_DIR}/${base_filename}.model.gz" ] &&
       [ -e "${TREES_DIR}/${base_filename}.log" ] &&
       [ -e "${TREES_DIR}/${base_filename}.splits.nex" ]; then
        echo -e "Existing iqtree output file for alignment: ${base_filename} \t >>> \t OK"
    else
        # If any output file is missing, run iqtree
        echo -ne "${base_filename} \t >>> \t iqtree phylogenetic tree "
        iqtree -s "${file}" -nt AUTO -bb 1000 -quiet

        # Check the exit status of iqtree
        if [ $? -eq 0 ]; then
            echo -e "\t >>> \t OK"
        else
            echo -e "\t >>> \t Error"
        fi
        
        #  Wait 3sec until the output files are correctly written
        sleep 3

        iqtree_files="${file}.*"

        # Move the IQ-TREE output files to the trees directory
        mv $iqtree_files "${TREES_DIR}/"
        
    fi
done

echo -e "\nThe trees have been generated and stored in the directory: ${TREES_DIR}"


# 7.COMMON DATASET IDENTIFICATION-------------------------------------------------------------------

# The script identifies the common dataset by finding the common IDs between the results of each database.


first_file=true
reference_ids=()
max_ids_count=0
reference_file=""

echo -e "\n############################### Common/Non-Common Dataset ####################################\n"

# Loop through each result file to find the common dataset
for result_file in "${SEQUENCES_DIR}"/rbh_*_sequences.fa; do
    # Extract the IDs from the file
    ids=($(sed -n '/^-/,/^-/p' "${result_file}" | grep -E '^(-|[A-Za-z0-9].*)$' | awk '{print $2}'))
    ids_count=${#ids[@]}

    # Check if the current file has the maximum number of IDs
    if [ "${ids_count}" -gt "${max_ids_count}" ]; then
        max_ids_count="${ids_count}"
        reference_file="${result_file}"
    fi

    # If it's the first file, set the reference IDs to the current IDs
    if [ "${first_file}" = true ]; then
        reference_ids=("${ids[@]}")
        first_file=false
    else
        # Find the common IDs between the current IDs and the reference IDs
        common_ids=()

        for id in "${reference_ids[@]}"; do
            if echo "${ids[@]}" | grep -qw "${id}"; then
                common_ids+=("${id}")
            fi
        done
        
        # Update the reference IDs with the common IDs
        reference_ids=("${common_ids[@]}")
    fi
done

# Check if any common IDs were found
if [ ${#reference_ids[@]} -eq 0 ]; then
    echo "No common dataset was found."
fi

# Initialize non-common IDs variable
non_common_ids=()

# Loop through each result file to find the non-common dataset
for result_file in "${SEQUENCES_DIR}"/rbh_*_sequences.fa; do
    # Extract the IDs from the file
    ids=($(sed -n '/^-/,/^-/p' "${result_file}" | grep -E '^(-|[A-Za-z0-9].*)$' | awk '{print $2}'))

    # Find the non-common IDs between the current IDs and the reference IDs
    for id in "${ids[@]}"; do
        if ! echo "${reference_ids[@]}" | grep -qw "${id}"; then
            non_common_ids+=("${id}")
        fi
    done
done

# Remove duplicates from non-common IDs
non_common_ids=($(printf "%s\n" "${non_common_ids[@]}" | sort -u))

# Print header row with number of IDs in each column
printf "%-30s %-30s\n" "Common Dataset ID (${#reference_ids[@]})" "Non-Common Dataset ID (${#non_common_ids[@]})"

# Print separator row
printf "%-30s %-30s\n" "----" "-----"

# Print common IDs in the first column and non-common IDs in the second column
printf "%s\n" "${reference_ids[@]}" | paste -d "   " - <(printf "%s\n" "${non_common_ids[@]}") | column -t -s "   "

echo -e "\nInformation: The consensus tree will be built from the common dataset"

DATASETS_DIR="${RESULTS_DIR}/04.datasets"

# Create a directory to store the datasets
mkdir -p "${DATASETS_DIR}"



# Initialize variables to store the lines
common_dataset=""
non_common_dataset=""

# Iterate over each sequence file in the directory
for sequence_file in "${SEQUENCES_DIR}"/rbh_*_sequences.fa; do
    # Extract the IDs and their corresponding identity values from the sequence file (columns 1, 2 and 4)
    ids_and_values=$(sed -n '/^--*$/,/^--*$/p' "${sequence_file}" | grep -E '^([A-Za-z0-9].*)$' | awk '{print $1, $2, $4}')

    # Loop through the extracted IDs and their identity values
    while read -r db id value; do
        # Check if the current ID belongs to the common or non-common dataset
        if grep -qw "${id}" <<< "${reference_ids[@]}"; then
            # Append the ID and its identity value to the common_dataset variable
            common_dataset+="$(echo "${db} ${id} ${value}")\n"
        elif grep -qw "${id}" <<< "${non_common_ids[@]}"; then
            # Append the ID and its identity value to the non_common_dataset variable
            non_common_dataset+="$(echo "${db} ${id} ${value}")\n"
        fi
    done <<< "${ids_and_values}"
done

# Sort the common_dataset and non_common_dataset variables by the second column
common_dataset_sorted=$(echo -e "${common_dataset}" | sort -k2,2)
non_common_dataset_sorted=$(echo -e "${non_common_dataset}" | sort -k2,2)

# Create header lines for common and non-common datasets files with their respective identity values
echo "DB ID_Common_Dataset Value_Identity" > "${DATASETS_DIR}/common_dataset.txt"
echo "${common_dataset_sorted}" >> "${DATASETS_DIR}/common_dataset.txt"

echo "DB ID_NonCommon_Dataset Value_Identity" > "${DATASETS_DIR}/non_common_dataset.txt"
echo "${non_common_dataset_sorted}" >> "${DATASETS_DIR}/non_common_dataset.txt"


# 8.CONSENSUS TREE CONSTRUCTION---------------------------------------------------------------------

# The script generates a consensus tree from the common dataset using `iqtree`.


# Check if any common IDs were found
if [ ${#reference_ids[@]} -eq 0 ]; then
    echo -e "No common dataset was found to create the consensus tree.\n"
    echo -e "\n########################################## Done ##############################################\n"
    exit 1
fi

echo -e "\n#################################### Consensus Tree ##########################################\n"

# Concatenate the .aln.treefile files of the common dataset into a single file
consensus_tree="${TREES_DIR}/consensus_tree.treefile"

# Create an array of .aln.treefile files corresponding to the identifiers of the common dataset
consensus_treefiles=()
for id in "${reference_ids[@]}"; do
    consensus_treefiles+=("${TREES_DIR}/${id}.aln.treefile")
done

# Empty the output file before concatenating
> "${consensus_tree}"

for common_dataset_tree_file in "${consensus_treefiles[@]}"; do
    if [ -f "${common_dataset_tree_file}" ]; then
        cat "${common_dataset_tree_file}" >> "${consensus_tree}"
    else
        echo "File not found: ${tree_file}"
        echo "Exiting..."
        exit 1
    fi
done

# Check if the consensus tree file already exists
if [ -f "${consensus_tree}.contree" ]; then
    echo -e "Consensus tree file already exists. Skipping step. \t >>> \t OK"
    echo -e "The $(basename "${consensus_tree}") file has been generated in the directory: ${TREES_DIR}"
else
    echo -ne "Generating consensus tree from common dataset"
    # Generate a consensus tree
    iqtree -quiet -con "${consensus_tree}" || { echo "iqtree failed. Exiting..."; exit 1; }
    echo -e "\t >>> \t OK"
    echo -e "The $(basename "${consensus_tree}") file has been generated in the directory: ${TREES_DIR}"
fi

echo -e "\n########################################## Done ##############################################\n"