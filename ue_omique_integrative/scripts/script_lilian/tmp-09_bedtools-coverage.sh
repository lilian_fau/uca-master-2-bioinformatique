#!/bin/bash
#
# samtools version 1.18
#
# bedtools version v2.31.0 
#
# performe coverage on bam file


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/mapping_data"

#--------------------------------------------------------------------------------------------------------------------------------

# Measure execution time
SECONDS=0  

# Search for mapping files per replicates and initiate output file names
infiles=$(find ${INPUT} -type f -name "SRR*map_trim_paired.bam")

echo "process coverage for files :"

# Print the list of input files
echo "${infiles}" | tr " " "\n"
echo "---------------------------------------------------------"
echo

# Loop through each BAM file and generate bigwig files for coverage
for bam in ${infiles}; do

    echo "making bigwig file for "$(basename "${bam}")" ..."

    # Set the output file path for bigwig
    outfile="${bam%.bam}_coverage.bw"

    # Sort BAM file using samtools and generate coverage in bedGraph format using bedtools genomecov
    samtools sort -@ 4 "${bam}" | bedtools genomecov -ibam - -bg > "${outfile}"

done

echo "---------------------------------------------------------"
echo
echo "files created :"

# List the created coverage bigwig files
ls ${INPUT}/SRR*map_trim_paired_coverage.bw

echo "_________________________________________________________"
echo nb threads : "${nbThreads}"
echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
