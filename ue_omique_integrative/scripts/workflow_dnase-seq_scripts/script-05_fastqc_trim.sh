#!/bin/bash
#
# fastqc version v0.12.1
#
# usage : give as first argument the number of thread to be used

echo "usage : give as first argument the number of thread to be used"
echo
echo -e "\t${0/*\/} nbThreads"

nbThreads=${1:-4}

echo
echo $0
echo 

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/trim_data"
OUTPUT="$WKDIR/trim_data"

CONDITION1="18h"
CONDITION2="control"

#--------------------------------------------------------------------------------------------------------------------------------

# Set the path for the metadata file in CSV format
metadata=$INPUT/Metadata_PRJNA304160_T0_T18.csv

# Extract specific fields from metadata based on conditions and filter by "DNase-Hypersensitivity;18h;control$"
sraIds=$(cat ${metadata} | cut -d';' -f1,2,34,35 | grep "DNase-Hypersensitivity;${CONDITION1};${CONDITION2}$" | cut -d';' -f1)

# Loop through the list of SRA IDs
for srr in ${sraIds}; do
        echo "_____________ start ${srr} _____________\n"

        # Measure execution time
        SECONDS=0 

        echo "Check for an empty directory..."
        # Check if the output directory exists
        if [ -d "${OUTPUT}" ]; then
                # Check if the specific SRA ID directory is not empty
                if [ "$(ls -A ${OUTPUT}/${srr})" ]; then
                        echo "in ${OUTPUT}"
                        echo "${srr}" is not empty and will be cleared
                        #rm -rf "${OUTPUT}/${srr}"
                else
                        echo "in ${OUTPUT}"
                        echo "${srr}" is empty
                fi
        else
                # Create the output directory if it doesn't exist
                mkdir -p -m 770 ${OUTPUT}
        fi

        # Create SRA ID specific output directory
        mkdir -p -m 770 ${OUTPUT}/${srr}	

        echo "run Quality control on raw data..."
        # Run FastQC for quality control on trim data
        fastqc ${INPUT}/${srr}/${srr}*.fastq.gz \
                --OUTPUT ${OUTPUT}/${srr} \
                --threads ${nbThreads}

        echo "files created :"
        # List files in the output directory
        ls ${OUTPUT}/${srr}

        echo -e "_____________ end ${srr} _____________"
        echo nb threads : "${nbThreads}"
        echo duration : duration $(($SECONDS / 60))min $(($SECONDS % 60))sec
        echo

        # Set permissions for the created files
        chmod 440 ${OUTPUT}/${srr}/* 
done

