import ftplib
import os
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed
import multiprocessing
import gzip
import subprocess

# Informations du serveur FTP d'Ensembl Plants
ftp_server = "ftp.ensemblgenomes.org"
ftp_directory = "/pub/release-59/plants/fasta/triticum_aestivum/pep/"
local_directory = "triticum_aestivum_genome/"
db_directory = "triticum_aestivum_db/"
db_name = "triticum_aestivum"

def create_local_directory(directory):
    """
    Crée le répertoire local si inexistant.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)

def file_already_downloaded(local_file):
    """
    Vérifie si le fichier local existe déjà.
    """
    return os.path.exists(local_file)

def download_file(ftp_server, ftp_directory, local_directory, remote_file):
    """
    Télécharge un fichier depuis le serveur FTP vers le répertoire local.
    """
    local_file = os.path.join(local_directory, os.path.basename(remote_file))
    if file_already_downloaded(local_file):
        print(f"{local_file} déjà téléchargé, saut du téléchargement.")
        return

    with ftplib.FTP(ftp_server) as ftp:
        ftp.login()
        ftp.cwd(ftp_directory)
        with open(local_file, 'wb') as lf:
            def callback(data):
                pbar.update(len(data))
                lf.write(data)
            
            ftp.voidcmd('TYPE I')
            total_size = ftp.size(remote_file)
            pbar = tqdm(total=total_size, unit='B', unit_scale=True, desc=os.path.basename(remote_file))
            
            ftp.retrbinary(f'RETR {remote_file}', callback)
            pbar.close()

def download_genome(ftp_server, ftp_directory, local_directory, file_patterns):
    """
    Télécharge les fichiers du génome correspondant aux motifs spécifiés depuis le serveur FTP.
    """
    create_local_directory(local_directory)
    
    with ftplib.FTP(ftp_server) as ftp:
        ftp.login()
        ftp.cwd(ftp_directory)
        
        files = ftp.nlst()
        files_to_download = [file for file in files if any(pattern in file for pattern in file_patterns)]
        
        num_workers = multiprocessing.cpu_count()
        with ThreadPoolExecutor(max_workers=num_workers) as executor:
            future_to_file = {executor.submit(download_file, ftp_server, ftp_directory, local_directory, file): file for file in files_to_download}
            for future in as_completed(future_to_file):
                file = future_to_file[future]
                try:
                    future.result()
                except Exception as exc:
                    print(f'{file} a généré une exception : {exc}')

def decompress_file(gz_file):
    """
    Décompresse un fichier .gz
    """
    decompressed_file = gz_file[:-3]
    if os.path.exists(decompressed_file):
        print(f"{decompressed_file} déjà décompressé, saut de la décompression.")
        return

    with gzip.open(gz_file, 'rb') as f_in:
        with open(decompressed_file, 'wb') as f_out:
            f_out.write(f_in.read())
    print(f'{gz_file} décompressé avec succès.')

def decompress_all_files(local_directory):
    """
    Décompresse tous les fichiers .gz dans le répertoire local.
    """
    gz_files = [os.path.join(local_directory, f) for f in os.listdir(local_directory) if f.endswith('.gz')]
    num_workers = multiprocessing.cpu_count()
    
    with ThreadPoolExecutor(max_workers=num_workers) as executor:
        future_to_file = {executor.submit(decompress_file, file): file for file in gz_files}
        for future in as_completed(future_to_file):
            file = future_to_file[future]
            try:
                future.result()
            except Exception as exc:
                print(f'{file} a généré une exception : {exc}')

def create_mmseqs2_db(local_directory, db_directory, db_name):
    """
    Crée une base de données MMseqs2 à partir des fichiers décompressés.
    """
    fasta_files = [os.path.join(local_directory, f) for f in os.listdir(local_directory) if f.endswith('.fa') or f.endswith('.fasta')]
    
    # Vérifier s'il y a plus d'un fichier à concaténer
    if len(fasta_files) > 1:
        concatenated_fasta = os.path.join(local_directory, "concatenated_genome.fasta")
        
        # Concaténer tous les fichiers fasta en un seul fichier
        with open(concatenated_fasta, 'w') as outfile:
            for fname in fasta_files:
                with open(fname) as infile:
                    outfile.write(infile.read())
        
        # Commande pour créer la base de données MMseqs2
        mmseqs2_create_db_cmd = [
            "mmseqs", "createdb", concatenated_fasta, os.path.join(db_directory, db_name)
        ]
        
        try:
            # Exécuter la commande
            subprocess.run(mmseqs2_create_db_cmd, check=True)
            print("Base de données MMseqs2 créée avec succès.")
            
            # Supprimer tous les fichiers .fa après la création de la base de données
            for fasta_file in fasta_files:
                os.remove(fasta_file)
                print(f"Fichier {fasta_file} supprimé avec succès.")
        
        except subprocess.CalledProcessError as e:
            print(f"Erreur lors de la création de la base de données MMseqs2 : {e}")
    
    elif len(fasta_files) == 1:
        single_fasta_file = fasta_files[0]
        
        # Commande pour créer la base de données MMseqs2
        mmseqs2_create_db_cmd = [
            "mmseqs", "createdb", single_fasta_file, os.path.join(db_directory, db_name)
        ]
        
        try:
            # Exécuter la commande
            subprocess.run(mmseqs2_create_db_cmd, check=True)
            print(f"Base de données MMseqs2 créée avec succès à partir de {single_fasta_file}.")
        
        except subprocess.CalledProcessError as e:
            print(f"Erreur lors de la création de la base de données MMseqs2 : {e}")
    
    else:
        print("Aucun fichier .fa ou .fasta trouvé pour créer la base de données MMseqs2.")

def adjust_directory_permissions(directory):
    """
    Ajuste les permissions du répertoire spécifié pour l'écriture.
    """
    try:
        os.makedirs(directory, exist_ok=True)
        os.chmod(directory, 0o755)  # Change les permissions du répertoire à 755
        print(f"Permissions du répertoire {directory} ajustées avec succès.")
    except Exception as e:
        print(f"Erreur lors de l'ajustement des permissions du répertoire {directory} : {e}")

def main():
    # Ajuster les permissions du répertoire de la base de données
    adjust_directory_permissions(db_directory)

    # Spécifiez ici les motifs des fichiers que vous souhaitez télécharger
    file_patterns = ["IWGSC.pep.all"]

    # Téléchargement des fichiers
    download_genome(ftp_server, ftp_directory, local_directory, file_patterns)
    
    # Décompression des fichiers
    decompress_all_files(local_directory)
    
    # Création de la base de données MMseqs2 et suppression des fichiers .fa si nécessaire
    create_mmseqs2_db(local_directory, db_directory, db_name)

if __name__ == "__main__":
    main()
