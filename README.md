![UCA logo](https://www.uca.fr/medias/photo/logo-uca-long-300dpi_1493730258077-png)

#### Author: Lilian Faurie - Master 2 Bioinformatique
#### Date: 29/01/2024

https://www.uca.fr/formation/nos-formations/par-ufr-ecoles-et-iut/institut-sciences-de-la-vie-de-la-sante-agronomie-environnement/ufr-de-biologie/master/master-bio-informatique
