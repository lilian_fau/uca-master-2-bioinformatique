#!/bin/bash

# README------------------------------------------------------------------------------------------

# Lilian Faurie - iGReD - 05/02/24

# This script downloads and processes sequences in .fasta format corresponding to the provided accession numbers from a supplementary file.
# It formats sequence files for immediate use in ColabFold Batch.
# For internship needs, the correspondence of accession numbers is made against the UniParc database.
# This script is inspired by part #1 of the LazyAF pipeline (McLean, Thomas C. "LazyAF, a Pipeline for Accessible Medium-Scale in Silico Prediction of Protein-Protein Interactions") rewritten in BASH and adapted to the internship requirements.
                                          
# CONFIG------------------------------------------------------------------------------------------

# Check if the input file is specified
if [ -z "$1" ]; then
    echo "Usage: $0 <accession_list_file>"
    exit 1
fi

# Check if the input file exists
if [ ! -f "$1" ]; then
    echo "Error: The input file $1 does not exist."
    exit 1
fi


# User input for the bait protein
echo -e "\n---- INPUTS ----------------------------------------------\n"
echo -e "Enter the name of the bait protein:"
read -p "> " bait_protein_name

echo -e "\nEnter the protein sequence of the bait protein:"
read -p "> " bait_protein_sequence
echo -e "\nNote: Length of the bait protein sequence: $(echo -n $bait_protein_sequence | wc -c) aa"
echo -e "\n---- DOWNLOAD --------------------------------------------"

# Generate a unique random identifier
unique_id=$(uuidgen | tr -d '-' | head -c 5)

# Destination folders for downloaded files
destination_folder="${bait_protein_name}_multimer_${unique_id}"
input_folder="${destination_folder}/input"
output_folder="${destination_folder}/input_fasta"

# Create the folders if they do not exist
mkdir -p "${destination_folder}" "${input_folder}" "${output_folder}"

# Database configuration
database="uniparc"

# LOGS--------------------------------------------------------------------------------------------

# Error logging
logfile="${destination_folder}/log.txt"
touch "$logfile"
exec > >(tee -a "$logfile") 2>&1

# DOWNLOAD & TREATMENTS---------------------------------------------------------------------------

# Function to retrieve protein sequences of genes
get_sequence() {
    local accession="$1"
    local url="https://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=${database}&id=${accession}&format=fasta&style=raw&Retrieve=Retrieve"
    local filename="${input_folder}/${accession}.fasta"

    # Download the sequence
    wget -q -O "$filename" "$url"

    # Check if the first line contains ">ID" using regex
    if grep -qE "^>[[:alnum:]]+" "$filename"; then
    
        # Remove "status=active" in all .fasta files except the current file + Add the accession number
        sed -i -E 's/status=.*//g; s/^>[[:alnum:]]+/& [gene='"${accession}"']/;' "$filename"

        # Use AWK to keep the longest sequence and the ID separately and store it in variables
        fasta=$(awk '/^>/ { if (length(seq) > length(longest_seq)) { longest_seq=seq; ID=id; } seq=""; id=$0; next; } { seq = seq $0; } END { if (length(seq) > length(longest_seq)) { longest_seq = seq; ID = id; } print longest_seq, ID }' "$filename")
        
        ID=$(echo "$fasta" | awk '{for (i=2; i<=NF; i++) printf "%s ", $i; print ""}')
        sequence=$(echo "$fasta" | awk '{print $1}')
        
        # Create the file with the longest sequence
        echo -e "$ID\n$sequence\n:$bait_protein_sequence" >> "${output_folder}/${accession}_x_${bait_protein_name}.fasta"

        # Concatenate all files into one
        echo -e "$ID\n$sequence" >> "${input_folder}/summary_sequences.fasta"
    fi
}

# FILES & VERIFICATIONS---------------------------------------------------------------------------

# List to store the names of successfully downloaded files
downloaded_files=()

# Use sort -u to extract unique accession numbers and avoid redundancy in database queries
unique_accessions=$(sort -u "$1")

# Display a message
echo -e "\nFile: $1"
echo -e "\nNote: Files will be downloaded to the folder ${output_folder}\n"

# Read each line of the file containing unique accession numbers and invoke get_sequence
while read -r accession; do
    echo "> $accession.fasta"
    get_sequence "$accession"
    
    file="${input_folder}/${accession}.fasta"
    
    if [ -e "$file" ] && grep -q "No entries found" "$file" || grep -q "ERROR" "$file" ; then
        echo -e "  Download: ERROR - the sequence does not exist in the ${database} database\n"
    else
        if [ -e "$file" ]; then
            echo -e "  Download:  OK\n"
            downloaded_files+=("$file")
        fi
    fi
done <<< "$unique_accessions"

file_count=$(find "${output_folder}" -type f | wc -l)

# Vérification des fichiers de sortie
echo -e "---- CHECKING ${file_count} OUTPUT FILES ----------------------------------------------------\n"

for final_file in "${output_folder}"/*.fasta; do
    # Extraire le texte après ":"
    text_after_colon=$(awk -F ':' '{print $2}' <<< "$(grep ":" "$final_file")")

    if [ -n "$text_after_colon" ]; then
        echo -e "$(basename "$final_file"): OK"
    else
        echo -e "$(basename "$final_file"): ERREUR - Aucune séquence n’a été fournie par l'utilisateur"
    fi
done

# Link to access the ColabFold batch processing
echo -e "\nNote: Load the folder ${output_folder} into https://colab.research.google.com/github/sokrypton/ColabFold/blob/main/batch/AlphaFold2_batch.ipynb to run batch processing of the data."