#!/bin/bash
#
# bedtools version v2.31.0 
#
# performe merge of peakcalling file 


echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT1="$WKDIR/consensus_dhs"
INPUT2="$WKDIR/intersect_data"

#--------------------------------------------------------------------------------------------------------------------------------

# input files
peaks_file="${INPUT1}/all_consensus_dhs.bed"
tfbs_file="${INPUT2}/ikzf1_tfbs_in_DHS.bed"

echo "input files :"
echo "peaks_file = $(ls $peaks_file)"
echo "tfbs_file= = $(ls $tfbs_file)"
echo


scope=5000

# output files
widened_peaks="${peaks_file%.bed}_scope-${scope}.bed"
widened_tfbs="${tfbs_file%.bed}_scope-${scope}.bed"

echo "output files :"
echo "$widened_peaks"
echo "$widened_tfbs"
echo

# widden DNase-seq merges peaks
cat "$peaks_file" |
	awk -v OFS="\t" -v LEN=$scope '{print $1, $2-LEN, $3+LEN, $4, $5, $6, $7, $8, $9, $10}' > "${widened_peaks}"

# widden tfbs in opened regions
cat "$tfbs_file" |
	awk -v FS="\t" -v OFS="\t" -v LEN=5000 '{print $1, ($2+LEN), ($3+LEN), $4, $5, $6, $7, $8, $9, $10}' > "${widened_tfbs}"

echo "Results for scope = ${scope}"
echo "output : "
ls ${widened_peaks}
ls ${widened_tfbs}
