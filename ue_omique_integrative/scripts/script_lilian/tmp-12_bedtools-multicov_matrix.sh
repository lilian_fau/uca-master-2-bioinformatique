#!/bin/bash
#
# bedtools version v2.31.0 
#
# perform bedtools multicov

echo
echo "$(basename ${0})"
echo

#WORKING DIRECTORY---------------------------------------------------------------------------------------------------------------

WKDIR="/home/data_local"
INPUT="$WKDIR/mapping_data"
OUTPUT="$WKDIR/matrix_data"

#--------------------------------------------------------------------------------------------------------------------------------

BAM_FILES=$(find $INPUT -type f -name "*.bam")
BED_FILE="$WKDIR/consensus_dhs/all_consensus_dhs_refseq.bed"


for BAM in "${BAM_FILES[@]}"; do
    echo $BAM
    samtools sort -@ 8 -o "$BAM" "$BAM"
    picard BuildBamIndex I=$BAM
done

echo -e "Processed BED file is: \n$BED_FILE\n"
echo -e "Processed BAM files are:\n$BAM_FILES"

outfile="$OUTPUT/matrix_counts.tsv"

# Multicoverage BED/BAMS
bedtools multicov -bams $(echo $BAM_FILES) -bed $BED_FILE > $outfile

